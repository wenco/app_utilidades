using System;
using System.Collections.Generic;
using System.Linq;

namespace api.Utils
{
    public static class CloneListUtil
    {
        public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone());
        }
    }
}