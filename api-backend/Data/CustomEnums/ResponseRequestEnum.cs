using System;

namespace api.Data.CustomEnums
{
    public enum ResponseRequestEnum
    {
        Success = 1,
        Warning = 2,
        Info = 3
    }
}
