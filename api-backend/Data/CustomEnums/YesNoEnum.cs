using System;

namespace api.Data.CustomEnums
{
    public enum YesNoEnum
    {
        Yes = 1,
        No = 0
    }
}
