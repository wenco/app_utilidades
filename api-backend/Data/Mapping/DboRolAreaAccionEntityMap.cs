using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboRolAreaAccionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboRolAreaAccionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboRolAreaAccionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("rol_area_accion", "dbo");

            // key
            builder.HasKey(t => new { t.IdRol, t.IdArea, t.IdAccion });

            // properties
            builder.Property(t => t.IdRol)
                .IsRequired()
                .HasColumnName("id_rol")
                .HasColumnType("int");

            builder.Property(t => t.IdArea)
                .IsRequired()
                .HasColumnName("id_area")
                .HasColumnType("int");

            builder.Property(t => t.IdAccion)
                .IsRequired()
                .HasColumnName("id_accion")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.FkRolAreaAccionAccion)
                .WithMany(t => t.FkRolAreaAccionAccionList)
                .HasForeignKey(d => d.IdAccion)
                .HasConstraintName("FK_rol_area_accion_accion");

            builder.HasOne(t => t.FkRolAreaAccionRol)
                .WithMany(t => t.FkRolAreaAccionRolList)
                .HasForeignKey(d => d.IdRol)
                .HasConstraintName("FK_rol_area_accion_rol");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "rol_area_accion";
        }

        public struct Columns
        {
            public const string IdRol = "id_rol";
            public const string IdArea = "id_area";
            public const string IdAccion = "id_accion";
        }
        #endregion
    }
}
