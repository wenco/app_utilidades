using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboRolEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboRolEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboRolEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("rol", "dbo");

            // key
            builder.HasKey(t => t.IdRol);

            // properties
            builder.Property(t => t.IdRol)
                .IsRequired()
                .HasColumnName("id_rol")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasColumnType("nvarchar(150)")
                .HasMaxLength(150);

            // relationships
            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "rol";
        }

        public struct Columns
        {
            public const string IdRol = "id_rol";
            public const string Nombre = "nombre";
        }
        #endregion
    }
}
