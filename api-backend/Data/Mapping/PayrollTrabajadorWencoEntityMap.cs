using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class PayrollTrabajadorWencoEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.PayrollTrabajadorWencoEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.PayrollTrabajadorWencoEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("trabajador_wenco", "payroll");

            // key
            builder.HasKey(t => t.IdTrabajadorWenco);

            // properties
            builder.Property(t => t.IdTrabajadorWenco)
                .IsRequired()
                .HasColumnName("id_trabajador_wenco")
                .HasColumnType("int");

            builder.Property(t => t.IdPayroll)
                .IsRequired()
                .HasColumnName("id_payroll")
                .HasColumnType("char(12)")
                .HasMaxLength(12);

            builder.Property(t => t.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasColumnType("nvarchar(350)")
                .HasMaxLength(350);

            builder.Property(t => t.IdCentroCosto)
                .HasColumnName("id_centro_costo")
                .HasColumnType("int");

            builder.Property(t => t.IdJefeTrabajador)
                .HasColumnName("id_jefe_trabajador")
                .HasColumnType("int");

            builder.Property(t => t.EsComiteParitario)
                .IsRequired()
                .HasColumnName("es_comite_paritario")
                .HasColumnType("int");

            builder.Property(t => t.Activo)
                .HasColumnName("activo")
                .HasColumnType("int");

            builder.Property(t => t.Categoria)
                .HasColumnName("categoria")
                .HasColumnType("int")
                .HasDefaultValueSql("((3))");

            builder.Property(t => t.Nombres)
                .HasColumnName("nombres")
                .HasColumnType("nvarchar(150)")
                .HasMaxLength(150);

            builder.Property(t => t.ApellidoPaterno)
                .HasColumnName("apellido_paterno")
                .HasColumnType("nvarchar(150)")
                .HasMaxLength(150);

            builder.Property(t => t.ApellidoMaterno)
                .HasColumnName("apellido_materno")
                .HasColumnType("nvarchar(150)")
                .HasMaxLength(150);

            builder.Property(t => t.NombrePila)
                .HasColumnName("nombre_pila")
                .HasColumnType("nvarchar(150)")
                .HasMaxLength(150);

            builder.Property(t => t.Cargo)
                .HasColumnName("cargo")
                .HasColumnType("int");

            builder.Property(t => t.IdEmpresa)
                .HasColumnName("id_empresa")
                .HasColumnType("int")
                .HasDefaultValueSql("((1))");

            builder.Property(t => t.FechaNacimiento)
                .HasColumnName("fecha_nacimiento")
                .HasColumnType("date");

            builder.Property(t => t.FechaIngreso)
                .HasColumnName("fecha_ingreso")
                .HasColumnType("date");

            builder.Property(t => t.Direccion)
                .HasColumnName("direccion")
                .HasColumnType("varchar(128)")
                .HasMaxLength(128);

            builder.Property(t => t.Telefono)
                .HasColumnName("telefono")
                .HasColumnType("varchar(18)")
                .HasMaxLength(18);

            builder.Property(t => t.MarcaAsistencia)
                .HasColumnName("marca_asistencia")
                .HasColumnType("int")
                .HasDefaultValueSql("((1))");

            builder.Property(t => t.PermiteHhee)
                .HasColumnName("permite_hhee")
                .HasColumnType("int")
                .HasDefaultValueSql("((0))");

            builder.Property(t => t.CodigoProblemaHuella)
                .HasColumnName("codigo_problema_huella")
                .HasColumnType("varchar(16)")
                .HasMaxLength(16);

            builder.Property(t => t.FechaCreacion)
                .HasColumnName("fecha_creacion")
                .HasColumnType("datetime");

            builder.Property(t => t.FechaEdicion)
                .HasColumnName("fecha_edicion")
                .HasColumnType("datetime");

            builder.Property(t => t.FechaEdicionEpoch)
                .HasColumnName("fecha_edicion_epoch")
                .HasColumnType("bigint");

            builder.Property(t => t.Clasif)
                .HasColumnName("clasif")
                .HasColumnType("int");

            builder.Property(t => t.xClasif)
                .HasColumnName("x_clasif")
                .HasColumnType("varchar(40)")
                .HasMaxLength(40);

            builder.Property(t => t.Confidencial)
                .HasColumnName("confidencial")
                .HasColumnType("bit");

            builder.Property(t => t.CodigoProblemaPuerta)
                .HasColumnName("codigo_problema_puerta")
                .HasColumnType("varchar(50)")
                .HasMaxLength(50);

            // relationships
            builder.HasOne(t => t.FkTrabajadorTrabajadorJefe)
                .WithMany(t => t.FkTrabajadorTrabajadorJefeList)
                .HasForeignKey(d => d.IdJefeTrabajador)
                .HasConstraintName("FK_trabajador_trabajador_jefe");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "payroll";
            public const string Name = "trabajador_wenco";
        }

        public struct Columns
        {
            public const string IdTrabajadorWenco = "id_trabajador_wenco";
            public const string IdPayroll = "id_payroll";
            public const string Nombre = "nombre";
            public const string IdCentroCosto = "id_centro_costo";
            public const string IdJefeTrabajador = "id_jefe_trabajador";
            public const string EsComiteParitario = "es_comite_paritario";
            public const string Activo = "activo";
            public const string Categoria = "categoria";
            public const string Nombres = "nombres";
            public const string ApellidoPaterno = "apellido_paterno";
            public const string ApellidoMaterno = "apellido_materno";
            public const string NombrePila = "nombre_pila";
            public const string Cargo = "cargo";
            public const string IdEmpresa = "id_empresa";
            public const string FechaNacimiento = "fecha_nacimiento";
            public const string FechaIngreso = "fecha_ingreso";
            public const string Direccion = "direccion";
            public const string Telefono = "telefono";
            public const string MarcaAsistencia = "marca_asistencia";
            public const string PermiteHhee = "permite_hhee";
            public const string CodigoProblemaHuella = "codigo_problema_huella";
            public const string FechaCreacion = "fecha_creacion";
            public const string FechaEdicion = "fecha_edicion";
            public const string FechaEdicionEpoch = "fecha_edicion_epoch";
            public const string Clasif = "clasif";
            public const string xClasif = "x_clasif";
            public const string Confidencial = "confidencial";
            public const string CodigoProblemaPuerta = "codigo_problema_puerta";
        }
        #endregion
    }
}
