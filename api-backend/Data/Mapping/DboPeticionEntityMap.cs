using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboPeticionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboPeticionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboPeticionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("peticion", "dbo");

            // key
            builder.HasKey(t => t.IdPeticion);

            // properties
            builder.Property(t => t.IdPeticion)
                .IsRequired()
                .HasColumnName("id_peticion")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.Controller)
                .IsRequired()
                .HasColumnName("controller")
                .HasColumnType("varchar(64)")
                .HasMaxLength(64);

            builder.Property(t => t.Action)
                .IsRequired()
                .HasColumnName("action")
                .HasColumnType("varchar(64)")
                .HasMaxLength(64);

            builder.Property(t => t.Method)
                .IsRequired()
                .HasColumnName("method")
                .HasColumnType("varchar(16)")
                .HasMaxLength(16);

            builder.Property(t => t.IdAplicacion)
                .IsRequired()
                .HasColumnName("id_aplicacion")
                .HasColumnType("int");

            builder.Property(t => t.Restriccion)
                .HasColumnName("restriccion")
                .HasColumnType("bit");

            // relationships
            builder.HasOne(t => t.FkPeticionAplicacion)
                .WithMany(t => t.FkPeticionAplicacionList)
                .HasForeignKey(d => d.IdAplicacion)
                .HasConstraintName("FK_peticion_aplicacion");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "peticion";
        }

        public struct Columns
        {
            public const string IdPeticion = "id_peticion";
            public const string Controller = "controller";
            public const string Action = "action";
            public const string Method = "method";
            public const string IdAplicacion = "id_aplicacion";
            public const string Restriccion = "restriccion";
        }
        #endregion
    }
}
