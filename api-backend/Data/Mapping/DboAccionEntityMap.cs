using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboAccionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboAccionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboAccionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("accion", "dbo");

            // key
            builder.HasKey(t => t.IdAccion);

            // properties
            builder.Property(t => t.IdAccion)
                .IsRequired()
                .HasColumnName("id_accion")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.IdAplicacion)
                .IsRequired()
                .HasColumnName("id_aplicacion")
                .HasColumnType("int");

            builder.Property(t => t.Controller)
                .HasColumnName("controller")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Action)
                .HasColumnName("action")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Resource)
                .HasColumnName("resource")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Caption)
                .IsRequired()
                .HasColumnName("caption")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Descripcion)
                .HasColumnName("descripcion")
                .HasColumnType("nvarchar(500)")
                .HasMaxLength(500);

            builder.Property(t => t.Activo)
                .HasColumnName("activo")
                .HasColumnType("int");

            builder.Property(t => t.Tipo)
                .HasColumnName("tipo")
                .HasColumnType("int");

            builder.Property(t => t.IdAccionSuper)
                .HasColumnName("id_accion_super")
                .HasColumnType("int");

            builder.Property(t => t.Orden)
                .HasColumnName("orden")
                .HasColumnType("int");

            builder.Property(t => t.Seleccionable)
                .HasColumnName("seleccionable")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.FkAccionAccionSuper)
                .WithMany(t => t.FkAccionAccionSuperList)
                .HasForeignKey(d => d.IdAccionSuper)
                .HasConstraintName("FK_accion_accion_super");

            builder.HasOne(t => t.FkAccionAplicacion)
                .WithMany(t => t.FkAccionAplicacionList)
                .HasForeignKey(d => d.IdAplicacion)
                .HasConstraintName("FK_accion_aplicacion");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "accion";
        }

        public struct Columns
        {
            public const string IdAccion = "id_accion";
            public const string IdAplicacion = "id_aplicacion";
            public const string Controller = "controller";
            public const string Action = "action";
            public const string Resource = "resource";
            public const string Caption = "caption";
            public const string Descripcion = "descripcion";
            public const string Activo = "activo";
            public const string Tipo = "tipo";
            public const string IdAccionSuper = "id_accion_super";
            public const string Orden = "orden";
            public const string Seleccionable = "seleccionable";
        }
        #endregion
    }
}
