using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboRolUsuarioAplicacionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboRolUsuarioAplicacionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboRolUsuarioAplicacionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("rol_usuario_aplicacion", "dbo");

            // key
            builder.HasKey(t => new { t.IdRol, t.IdUsuario, t.IdAplicacion });

            // properties
            builder.Property(t => t.IdRol)
                .IsRequired()
                .HasColumnName("id_rol")
                .HasColumnType("int");

            builder.Property(t => t.IdUsuario)
                .IsRequired()
                .HasColumnName("id_usuario")
                .HasColumnType("int");

            builder.Property(t => t.IdAplicacion)
                .IsRequired()
                .HasColumnName("id_aplicacion")
                .HasColumnType("int");

            builder.Property(t => t.CantidadAccesoUsuario)
                .HasColumnName("cantidad_acceso_usuario")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.FkRolUsuarioAplicacionAplicacion)
                .WithMany(t => t.FkRolUsuarioAplicacionAplicacionList)
                .HasForeignKey(d => d.IdAplicacion)
                .HasConstraintName("FK_rol_usuario_aplicacion_aplicacion");

            builder.HasOne(t => t.FkRolUsuarioAplicacionRol)
                .WithMany(t => t.FkRolUsuarioAplicacionRolList)
                .HasForeignKey(d => d.IdRol)
                .HasConstraintName("FK_rol_usuario_aplicacion_rol");

            builder.HasOne(t => t.FkRolUsuarioAplicacionUsuario)
                .WithMany(t => t.FkRolUsuarioAplicacionUsuarioList)
                .HasForeignKey(d => d.IdUsuario)
                .HasConstraintName("FK_rol_usuario_aplicacion_usuario");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "rol_usuario_aplicacion";
        }

        public struct Columns
        {
            public const string IdRol = "id_rol";
            public const string IdUsuario = "id_usuario";
            public const string IdAplicacion = "id_aplicacion";
            public const string CantidadAccesoUsuario = "cantidad_acceso_usuario";
        }
        #endregion
    }
}
