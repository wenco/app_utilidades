using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboAplicacionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboAplicacionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboAplicacionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("aplicacion", "dbo");

            // key
            builder.HasKey(t => t.IdAplicacion);

            // properties
            builder.Property(t => t.IdAplicacion)
                .IsRequired()
                .HasColumnName("id_aplicacion")
                .HasColumnType("int");

            builder.Property(t => t.Alias)
                .IsRequired()
                .HasColumnName("alias")
                .HasColumnType("nvarchar(20)")
                .HasMaxLength(20);

            builder.Property(t => t.Titulo)
                .IsRequired()
                .HasColumnName("titulo")
                .HasColumnType("nvarchar(100)")
                .HasMaxLength(100);

            builder.Property(t => t.Descripcion)
                .HasColumnName("descripcion")
                .HasColumnType("nvarchar(1000)")
                .HasMaxLength(1000);

            builder.Property(t => t.Imagen)
                .HasColumnName("imagen")
                .HasColumnType("nvarchar(255)")
                .HasMaxLength(255);

            builder.Property(t => t.Visible)
                .HasColumnName("visible")
                .HasColumnType("int");

            builder.Property(t => t.Arquitectura)
                .IsRequired()
                .HasColumnName("arquitectura")
                .HasColumnType("int");

            builder.Property(t => t.BaseApi)
                .HasColumnName("base_api")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.RutaDistFrontend)
                .HasColumnName("ruta_dist_frontend")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.RutaDistBackend)
                .HasColumnName("ruta_dist_backend")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.BackendAppPool)
                .HasColumnName("backend_app_pool")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.FrontendAppPool)
                .HasColumnName("frontend_app_pool")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.ComputerName)
                .HasColumnName("computer_name")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.EsMovil)
                .HasColumnName("es_movil")
                .HasColumnType("int");

            builder.Property(t => t.NgVersion)
                .HasColumnName("ng_version")
                .HasColumnType("nvarchar(10)")
                .HasMaxLength(10);

            builder.Property(t => t.RutaRespaldoFrontend)
                .HasColumnName("ruta_respaldo_frontend")
                .HasColumnType("nvarchar(max)");

            builder.Property(t => t.RutaRespaldoBackend)
                .HasColumnName("ruta_respaldo_backend")
                .HasColumnType("nvarchar(max)");

            // relationships
            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "aplicacion";
        }

        public struct Columns
        {
            public const string IdAplicacion = "id_aplicacion";
            public const string Alias = "alias";
            public const string Titulo = "titulo";
            public const string Descripcion = "descripcion";
            public const string Imagen = "imagen";
            public const string Visible = "visible";
            public const string Arquitectura = "arquitectura";
            public const string BaseApi = "base_api";
            public const string RutaDistFrontend = "ruta_dist_frontend";
            public const string RutaDistBackend = "ruta_dist_backend";
            public const string BackendAppPool = "backend_app_pool";
            public const string FrontendAppPool = "frontend_app_pool";
            public const string ComputerName = "computer_name";
            public const string EsMovil = "es_movil";
            public const string NgVersion = "ng_version";
            public const string RutaRespaldoFrontend = "ruta_respaldo_frontend";
            public const string RutaRespaldoBackend = "ruta_respaldo_backend";
        }
        #endregion
    }
}
