using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboAccionPeticionEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboAccionPeticionEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboAccionPeticionEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("accion_peticion", "dbo");

            // key
            builder.HasKey(t => t.IdAccionPeticion);

            // properties
            builder.Property(t => t.IdAccionPeticion)
                .IsRequired()
                .HasColumnName("id_accion_peticion")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.IdAccion)
                .IsRequired()
                .HasColumnName("id_accion")
                .HasColumnType("int");

            builder.Property(t => t.IdPeticion)
                .IsRequired()
                .HasColumnName("id_peticion")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.FkAccionPeticionAccion)
                .WithMany(t => t.FkAccionPeticionAccionList)
                .HasForeignKey(d => d.IdAccion)
                .HasConstraintName("FK_accion_peticion_accion");

            builder.HasOne(t => t.FkAccionPeticionPeticion)
                .WithMany(t => t.FkAccionPeticionPeticionList)
                .HasForeignKey(d => d.IdPeticion)
                .HasConstraintName("FK_accion_peticion_peticion");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "accion_peticion";
        }

        public struct Columns
        {
            public const string IdAccionPeticion = "id_accion_peticion";
            public const string IdAccion = "id_accion";
            public const string IdPeticion = "id_peticion";
        }
        #endregion
    }
}
