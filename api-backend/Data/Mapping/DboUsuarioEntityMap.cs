using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboUsuarioEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboUsuarioEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboUsuarioEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("usuario", "dbo");

            // key
            builder.HasKey(t => t.IdUsuario);

            // properties
            builder.Property(t => t.IdUsuario)
                .IsRequired()
                .HasColumnName("id_usuario")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.IdTrabajadorWenco)
                .IsRequired()
                .HasColumnName("id_trabajador_wenco")
                .HasColumnType("int");

            builder.Property(t => t.IdEmpresaLogin)
                .HasColumnName("id_empresa_login")
                .HasColumnType("int");

            builder.Property(t => t.AdSamAccountName)
                .IsRequired()
                .HasColumnName("ad_sam_account_name")
                .HasColumnType("nvarchar(30)")
                .HasMaxLength(30);

            builder.Property(t => t.Email)
                .HasColumnName("email")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IdArea)
                .HasColumnName("id_area")
                .HasColumnType("int");

            builder.Property(t => t.Activo)
                .HasColumnName("activo")
                .HasColumnType("int")
                .HasDefaultValueSql("((0))");

            builder.Property(t => t.SocIdPerfilEspecial)
                .HasColumnName("soc_id_perfil_especial")
                .HasColumnType("int");

            builder.Property(t => t.Token)
                .HasColumnName("token")
                .HasColumnType("nvarchar(255)")
                .HasMaxLength(255);

            builder.Property(t => t.FechaExpiracion)
                .HasColumnName("fecha_expiracion")
                .HasColumnType("datetime");

            builder.Property(t => t.TokenPublico)
                .HasColumnName("token_publico")
                .HasColumnType("nvarchar(255)")
                .HasMaxLength(255);

            builder.Property(t => t.RequiereHuellaInactividad)
                .HasColumnName("requiere_huella_inactividad")
                .HasColumnType("bit");

            // relationships
            builder.HasOne(t => t.FkUsuarioEmpresa)
                .WithMany(t => t.FkUsuarioEmpresaList)
                .HasForeignKey(d => d.IdEmpresaLogin)
                .HasConstraintName("FK_usuario_empresa");

            builder.HasOne(t => t.FkUsuarioTrabajadorWenco)
                .WithMany(t => t.FkUsuarioTrabajadorWencoList)
                .HasForeignKey(d => d.IdTrabajadorWenco)
                .HasConstraintName("FK_usuario_trabajador_wenco");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "usuario";
        }

        public struct Columns
        {
            public const string IdUsuario = "id_usuario";
            public const string IdTrabajadorWenco = "id_trabajador_wenco";
            public const string IdEmpresaLogin = "id_empresa_login";
            public const string AdSamAccountName = "ad_sam_account_name";
            public const string Email = "email";
            public const string IdArea = "id_area";
            public const string Activo = "activo";
            public const string SocIdPerfilEspecial = "soc_id_perfil_especial";
            public const string Token = "token";
            public const string FechaExpiracion = "fecha_expiracion";
            public const string TokenPublico = "token_publico";
            public const string RequiereHuellaInactividad = "requiere_huella_inactividad";
        }
        #endregion
    }
}
