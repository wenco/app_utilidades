using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboEmpresaEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboEmpresaEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboEmpresaEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("empresa", "dbo");

            // key
            builder.HasKey(t => t.IdEmpresa);

            // properties
            builder.Property(t => t.IdEmpresa)
                .IsRequired()
                .HasColumnName("id_empresa")
                .HasColumnType("int");

            builder.Property(t => t.Descripcion)
                .IsRequired()
                .HasColumnName("descripcion")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.ConnectionString)
                .HasColumnName("ConnectionString")
                .HasColumnType("varchar(256)")
                .HasMaxLength(256);

            builder.Property(t => t.Dominio)
                .IsRequired()
                .HasColumnName("dominio")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Nombre)
                .HasColumnName("nombre")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IpServidorNotificacion)
                .HasColumnName("ip_servidor_notificacion")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.PuertoServidorNotificacion)
                .HasColumnName("puerto_servidor_notificacion")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IpServidorRasSincronizador)
                .HasColumnName("ip_servidor_ras_sincronizador")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.BaseDatosPayroll)
                .HasColumnName("base_datos_payroll")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.PuertoServidorRasSincronizador)
                .HasColumnName("puerto_servidor_ras_sincronizador")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.ServidorSistemas)
                .IsRequired()
                .HasColumnName("servidor_sistemas")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.DbSistemas)
                .IsRequired()
                .HasColumnName("db_sistemas")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.UrlBackoffice)
                .IsRequired()
                .HasColumnName("url_backoffice")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Nacionalidad)
                .HasColumnName("nacionalidad")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.RazonSocial)
                .HasColumnName("razon_social")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IdEmpresaTrabajador)
                .HasColumnName("id_empresa_trabajador")
                .HasColumnType("int");

            builder.Property(t => t.Rut)
                .HasColumnName("rut")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Direccion)
                .HasColumnName("direccion")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.FechaCreacion)
                .HasColumnName("fecha_creacion")
                .HasColumnType("datetime");

            builder.Property(t => t.FechaEdicion)
                .HasColumnName("fecha_edicion")
                .HasColumnType("datetime");

            builder.Property(t => t.FechaEdicionEpoch)
                .HasColumnName("fecha_edicion_epoch")
                .HasColumnType("bigint");

            builder.Property(t => t.LinkedServer)
                .IsRequired()
                .HasColumnName("linked_server")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IdEmpresaCg)
                .HasColumnName("id_empresa_cg")
                .HasColumnType("int");

            builder.Property(t => t.NombrePublicador)
                .HasColumnName("nombre_publicador")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.EmpresaActual)
                .HasColumnName("empresa_actual")
                .HasColumnType("int");

            builder.Property(t => t.LinkedServerErp)
                .HasColumnName("linked_server_erp")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.DbErp)
                .HasColumnName("db_erp")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.IdEmpresaTrabajadorChile)
                .HasColumnName("id_empresa_trabajador_chile")
                .HasColumnType("int");

            builder.Property(t => t.IdEmpresaTrabajadorPeru)
                .HasColumnName("id_empresa_trabajador_peru")
                .HasColumnType("int");

            builder.Property(t => t.TipoSwRrhh)
                .HasColumnName("tipo_sw_rrhh")
                .HasColumnType("int");

            builder.Property(t => t.LinkedServerSwRrhh)
                .HasColumnName("linked_server_sw_rrhh")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.DbSwRrhh)
                .HasColumnName("db_sw_rrhh")
                .HasColumnType("nvarchar(50)")
                .HasMaxLength(50);

            builder.Property(t => t.Giro)
                .HasColumnName("giro")
                .HasColumnType("nvarchar(255)")
                .HasMaxLength(255);

            builder.Property(t => t.UrlBackend)
                .HasColumnName("url_backend")
                .HasColumnType("varchar(100)")
                .HasMaxLength(100);

            builder.Property(t => t.GpProduccion)
                .HasColumnName("gp_produccion")
                .HasColumnType("int");

            builder.Property(t => t.CorreoDte)
                .HasColumnName("correo_dte")
                .HasColumnType("varchar(256)")
                .HasMaxLength(256);

            builder.Property(t => t.EmpresaDte)
                .HasColumnName("empresa_dte")
                .HasColumnType("varchar(30)")
                .HasMaxLength(30);

            // relationships
            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "empresa";
        }

        public struct Columns
        {
            public const string IdEmpresa = "id_empresa";
            public const string Descripcion = "descripcion";
            public const string ConnectionString = "ConnectionString";
            public const string Dominio = "dominio";
            public const string Nombre = "nombre";
            public const string IpServidorNotificacion = "ip_servidor_notificacion";
            public const string PuertoServidorNotificacion = "puerto_servidor_notificacion";
            public const string IpServidorRasSincronizador = "ip_servidor_ras_sincronizador";
            public const string BaseDatosPayroll = "base_datos_payroll";
            public const string PuertoServidorRasSincronizador = "puerto_servidor_ras_sincronizador";
            public const string ServidorSistemas = "servidor_sistemas";
            public const string DbSistemas = "db_sistemas";
            public const string UrlBackoffice = "url_backoffice";
            public const string Nacionalidad = "nacionalidad";
            public const string RazonSocial = "razon_social";
            public const string IdEmpresaTrabajador = "id_empresa_trabajador";
            public const string Rut = "rut";
            public const string Direccion = "direccion";
            public const string FechaCreacion = "fecha_creacion";
            public const string FechaEdicion = "fecha_edicion";
            public const string FechaEdicionEpoch = "fecha_edicion_epoch";
            public const string LinkedServer = "linked_server";
            public const string IdEmpresaCg = "id_empresa_cg";
            public const string NombrePublicador = "nombre_publicador";
            public const string EmpresaActual = "empresa_actual";
            public const string LinkedServerErp = "linked_server_erp";
            public const string DbErp = "db_erp";
            public const string IdEmpresaTrabajadorChile = "id_empresa_trabajador_chile";
            public const string IdEmpresaTrabajadorPeru = "id_empresa_trabajador_peru";
            public const string TipoSwRrhh = "tipo_sw_rrhh";
            public const string LinkedServerSwRrhh = "linked_server_sw_rrhh";
            public const string DbSwRrhh = "db_sw_rrhh";
            public const string Giro = "giro";
            public const string UrlBackend = "url_backend";
            public const string GpProduccion = "gp_produccion";
            public const string CorreoDte = "correo_dte";
            public const string EmpresaDte = "empresa_dte";
        }
        #endregion
    }
}
