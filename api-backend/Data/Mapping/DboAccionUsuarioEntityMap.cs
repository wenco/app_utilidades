using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace api.Data.Mapping
{
    public partial class DboAccionUsuarioEntityMap
        : IEntityTypeConfiguration<api.Data.Entities.DboAccionUsuarioEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<api.Data.Entities.DboAccionUsuarioEntity> builder)
        {
            #region Generated Configure
            // table
            builder.ToTable("accion_usuario", "dbo");

            // key
            builder.HasKey(t => t.IdAccionUsuario);

            // properties
            builder.Property(t => t.IdAccionUsuario)
                .IsRequired()
                .HasColumnName("id_accion_usuario")
                .HasColumnType("int")
                .ValueGeneratedOnAdd();

            builder.Property(t => t.IdAccion)
                .IsRequired()
                .HasColumnName("id_accion")
                .HasColumnType("int");

            builder.Property(t => t.IdUsuario)
                .IsRequired()
                .HasColumnName("id_usuario")
                .HasColumnType("int");

            builder.Property(t => t.TienePermiso)
                .IsRequired()
                .HasColumnName("tiene_permiso")
                .HasColumnType("int");

            // relationships
            builder.HasOne(t => t.FkAccionUsuarioAccion)
                .WithMany(t => t.FkAccionUsuarioAccionList)
                .HasForeignKey(d => d.IdAccion)
                .HasConstraintName("FK_accion_usuario_accion");

            builder.HasOne(t => t.FkAccionUsuarioUsuario)
                .WithMany(t => t.FkAccionUsuarioUsuarioList)
                .HasForeignKey(d => d.IdUsuario)
                .HasConstraintName("FK_accion_usuario_usuario");

            #endregion
        }

        #region Generated Constants
        public struct Table
        {
            public const string Schema = "dbo";
            public const string Name = "accion_usuario";
        }

        public struct Columns
        {
            public const string IdAccionUsuario = "id_accion_usuario";
            public const string IdAccion = "id_accion";
            public const string IdUsuario = "id_usuario";
            public const string TienePermiso = "tiene_permiso";
        }
        #endregion
    }
}
