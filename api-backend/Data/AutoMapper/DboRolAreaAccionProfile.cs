using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboRolAreaAccionProfile
        : Profile
    {
        public DboRolAreaAccionProfile()
        {
            CreateMap<api.Data.Entities.DboRolAreaAccionEntity, api.Data.Models.DboRolAreaAccionReadModel>();

            CreateMap<api.Data.Models.DboRolAreaAccionCreateModel, api.Data.Entities.DboRolAreaAccionEntity>();

            CreateMap<api.Data.Entities.DboRolAreaAccionEntity, api.Data.Models.DboRolAreaAccionUpdateModel>();

            CreateMap<api.Data.Models.DboRolAreaAccionUpdateModel, api.Data.Entities.DboRolAreaAccionEntity>();

            CreateMap<api.Data.Models.DboRolAreaAccionReadModel, api.Data.Models.DboRolAreaAccionUpdateModel>();

        }

    }
}
