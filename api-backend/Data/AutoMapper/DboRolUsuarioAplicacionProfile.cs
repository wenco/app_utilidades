using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboRolUsuarioAplicacionProfile
        : Profile
    {
        public DboRolUsuarioAplicacionProfile()
        {
            CreateMap<api.Data.Entities.DboRolUsuarioAplicacionEntity, api.Data.Models.DboRolUsuarioAplicacionReadModel>();

            CreateMap<api.Data.Models.DboRolUsuarioAplicacionCreateModel, api.Data.Entities.DboRolUsuarioAplicacionEntity>();

            CreateMap<api.Data.Entities.DboRolUsuarioAplicacionEntity, api.Data.Models.DboRolUsuarioAplicacionUpdateModel>();

            CreateMap<api.Data.Models.DboRolUsuarioAplicacionUpdateModel, api.Data.Entities.DboRolUsuarioAplicacionEntity>();

            CreateMap<api.Data.Models.DboRolUsuarioAplicacionReadModel, api.Data.Models.DboRolUsuarioAplicacionUpdateModel>();

        }

    }
}
