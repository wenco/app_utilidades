using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboEmpresaProfile
        : Profile
    {
        public DboEmpresaProfile()
        {
            CreateMap<api.Data.Entities.DboEmpresaEntity, api.Data.Models.DboEmpresaReadModel>();

            CreateMap<api.Data.Models.DboEmpresaCreateModel, api.Data.Entities.DboEmpresaEntity>();

            CreateMap<api.Data.Entities.DboEmpresaEntity, api.Data.Models.DboEmpresaUpdateModel>();

            CreateMap<api.Data.Models.DboEmpresaUpdateModel, api.Data.Entities.DboEmpresaEntity>();

            CreateMap<api.Data.Models.DboEmpresaReadModel, api.Data.Models.DboEmpresaUpdateModel>();

        }

    }
}
