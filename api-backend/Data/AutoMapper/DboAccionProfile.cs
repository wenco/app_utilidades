using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboAccionProfile
        : Profile
    {
        public DboAccionProfile()
        {
            CreateMap<api.Data.Entities.DboAccionEntity, api.Data.Models.DboAccionReadModel>();

            CreateMap<api.Data.Models.DboAccionCreateModel, api.Data.Entities.DboAccionEntity>();

            CreateMap<api.Data.Entities.DboAccionEntity, api.Data.Models.DboAccionUpdateModel>();

            CreateMap<api.Data.Models.DboAccionUpdateModel, api.Data.Entities.DboAccionEntity>();

            CreateMap<api.Data.Models.DboAccionReadModel, api.Data.Models.DboAccionUpdateModel>();

        }

    }
}
