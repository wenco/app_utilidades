using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class PayrollTrabajadorWencoProfile
        : Profile
    {
        public PayrollTrabajadorWencoProfile()
        {
            CreateMap<api.Data.Entities.PayrollTrabajadorWencoEntity, api.Data.Models.PayrollTrabajadorWencoReadModel>();

            CreateMap<api.Data.Models.PayrollTrabajadorWencoCreateModel, api.Data.Entities.PayrollTrabajadorWencoEntity>();

            CreateMap<api.Data.Entities.PayrollTrabajadorWencoEntity, api.Data.Models.PayrollTrabajadorWencoUpdateModel>();

            CreateMap<api.Data.Models.PayrollTrabajadorWencoUpdateModel, api.Data.Entities.PayrollTrabajadorWencoEntity>();

            CreateMap<api.Data.Models.PayrollTrabajadorWencoReadModel, api.Data.Models.PayrollTrabajadorWencoUpdateModel>();

        }

    }
}
