using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboUsuarioProfile
        : Profile
    {
        public DboUsuarioProfile()
        {
            CreateMap<api.Data.Entities.DboUsuarioEntity, api.Data.Models.DboUsuarioReadModel>();

            CreateMap<api.Data.Models.DboUsuarioCreateModel, api.Data.Entities.DboUsuarioEntity>();

            CreateMap<api.Data.Entities.DboUsuarioEntity, api.Data.Models.DboUsuarioUpdateModel>();

            CreateMap<api.Data.Models.DboUsuarioUpdateModel, api.Data.Entities.DboUsuarioEntity>();

            CreateMap<api.Data.Models.DboUsuarioReadModel, api.Data.Models.DboUsuarioUpdateModel>();

        }

    }
}
