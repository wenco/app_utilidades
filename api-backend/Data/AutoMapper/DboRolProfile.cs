using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboRolProfile
        : Profile
    {
        public DboRolProfile()
        {
            CreateMap<api.Data.Entities.DboRolEntity, api.Data.Models.DboRolReadModel>();

            CreateMap<api.Data.Models.DboRolCreateModel, api.Data.Entities.DboRolEntity>();

            CreateMap<api.Data.Entities.DboRolEntity, api.Data.Models.DboRolUpdateModel>();

            CreateMap<api.Data.Models.DboRolUpdateModel, api.Data.Entities.DboRolEntity>();

            CreateMap<api.Data.Models.DboRolReadModel, api.Data.Models.DboRolUpdateModel>();

        }

    }
}
