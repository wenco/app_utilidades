using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboAplicacionProfile
        : Profile
    {
        public DboAplicacionProfile()
        {
            CreateMap<api.Data.Entities.DboAplicacionEntity, api.Data.Models.DboAplicacionReadModel>();

            CreateMap<api.Data.Models.DboAplicacionCreateModel, api.Data.Entities.DboAplicacionEntity>();

            CreateMap<api.Data.Entities.DboAplicacionEntity, api.Data.Models.DboAplicacionUpdateModel>();

            CreateMap<api.Data.Models.DboAplicacionUpdateModel, api.Data.Entities.DboAplicacionEntity>();

            CreateMap<api.Data.Models.DboAplicacionReadModel, api.Data.Models.DboAplicacionUpdateModel>();

        }

    }
}
