using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboAccionUsuarioProfile
        : Profile
    {
        public DboAccionUsuarioProfile()
        {
            CreateMap<api.Data.Entities.DboAccionUsuarioEntity, api.Data.Models.DboAccionUsuarioReadModel>();

            CreateMap<api.Data.Models.DboAccionUsuarioCreateModel, api.Data.Entities.DboAccionUsuarioEntity>();

            CreateMap<api.Data.Entities.DboAccionUsuarioEntity, api.Data.Models.DboAccionUsuarioUpdateModel>();

            CreateMap<api.Data.Models.DboAccionUsuarioUpdateModel, api.Data.Entities.DboAccionUsuarioEntity>();

            CreateMap<api.Data.Models.DboAccionUsuarioReadModel, api.Data.Models.DboAccionUsuarioUpdateModel>();

        }

    }
}
