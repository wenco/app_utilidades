using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboPeticionProfile
        : Profile
    {
        public DboPeticionProfile()
        {
            CreateMap<api.Data.Entities.DboPeticionEntity, api.Data.Models.DboPeticionReadModel>();

            CreateMap<api.Data.Models.DboPeticionCreateModel, api.Data.Entities.DboPeticionEntity>();

            CreateMap<api.Data.Entities.DboPeticionEntity, api.Data.Models.DboPeticionUpdateModel>();

            CreateMap<api.Data.Models.DboPeticionUpdateModel, api.Data.Entities.DboPeticionEntity>();

            CreateMap<api.Data.Models.DboPeticionReadModel, api.Data.Models.DboPeticionUpdateModel>();

        }

    }
}
