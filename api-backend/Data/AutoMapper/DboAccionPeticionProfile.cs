using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.AutoMapper
{
    public partial class DboAccionPeticionProfile
        : Profile
    {
        public DboAccionPeticionProfile()
        {
            CreateMap<api.Data.Entities.DboAccionPeticionEntity, api.Data.Models.DboAccionPeticionReadModel>();

            CreateMap<api.Data.Models.DboAccionPeticionCreateModel, api.Data.Entities.DboAccionPeticionEntity>();

            CreateMap<api.Data.Entities.DboAccionPeticionEntity, api.Data.Models.DboAccionPeticionUpdateModel>();

            CreateMap<api.Data.Models.DboAccionPeticionUpdateModel, api.Data.Entities.DboAccionPeticionEntity>();

            CreateMap<api.Data.Models.DboAccionPeticionReadModel, api.Data.Models.DboAccionPeticionUpdateModel>();

        }

    }
}
