using System;
using AutoMapper;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.CustomMapper
{
    public partial class UsuarioAuthProfile
        : Profile
    {
        public UsuarioAuthProfile()
        {
            CreateMap<api.Data.Entities.DboUsuarioEntity, api.Data.CustomModels.UsuarioAuthModel>();

            CreateMap<api.Data.CustomModels.UsuarioAuthModel, api.Data.Entities.DboUsuarioEntity>();

        }

    }
}
