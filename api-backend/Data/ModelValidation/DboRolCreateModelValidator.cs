using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboRolCreateModelValidator
        : AbstractValidator<DboRolCreateModel>
    {
        public DboRolCreateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Nombre).NotEmpty();
            RuleFor(p => p.Nombre).MaximumLength(150);
            #endregion
        }

    }
}
