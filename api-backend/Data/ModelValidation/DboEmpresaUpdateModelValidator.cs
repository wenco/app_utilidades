using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboEmpresaUpdateModelValidator
        : AbstractValidator<DboEmpresaUpdateModel>
    {
        public DboEmpresaUpdateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Descripcion).NotEmpty();
            RuleFor(p => p.Descripcion).MaximumLength(50);
            RuleFor(p => p.ConnectionString).MaximumLength(256);
            RuleFor(p => p.Dominio).NotEmpty();
            RuleFor(p => p.Dominio).MaximumLength(50);
            RuleFor(p => p.Nombre).MaximumLength(50);
            RuleFor(p => p.IpServidorNotificacion).MaximumLength(50);
            RuleFor(p => p.PuertoServidorNotificacion).MaximumLength(50);
            RuleFor(p => p.IpServidorRasSincronizador).MaximumLength(50);
            RuleFor(p => p.BaseDatosPayroll).MaximumLength(50);
            RuleFor(p => p.PuertoServidorRasSincronizador).MaximumLength(50);
            RuleFor(p => p.ServidorSistemas).NotEmpty();
            RuleFor(p => p.ServidorSistemas).MaximumLength(50);
            RuleFor(p => p.DbSistemas).NotEmpty();
            RuleFor(p => p.DbSistemas).MaximumLength(50);
            RuleFor(p => p.UrlBackoffice).NotEmpty();
            RuleFor(p => p.UrlBackoffice).MaximumLength(50);
            RuleFor(p => p.Nacionalidad).MaximumLength(50);
            RuleFor(p => p.RazonSocial).MaximumLength(50);
            RuleFor(p => p.Rut).MaximumLength(50);
            RuleFor(p => p.Direccion).MaximumLength(50);
            RuleFor(p => p.LinkedServer).NotEmpty();
            RuleFor(p => p.LinkedServer).MaximumLength(50);
            RuleFor(p => p.NombrePublicador).MaximumLength(50);
            RuleFor(p => p.LinkedServerErp).MaximumLength(50);
            RuleFor(p => p.DbErp).MaximumLength(50);
            RuleFor(p => p.LinkedServerSwRrhh).MaximumLength(50);
            RuleFor(p => p.DbSwRrhh).MaximumLength(50);
            RuleFor(p => p.Giro).MaximumLength(255);
            RuleFor(p => p.UrlBackend).MaximumLength(100);
            RuleFor(p => p.CorreoDte).MaximumLength(256);
            RuleFor(p => p.EmpresaDte).MaximumLength(30);
            #endregion
        }

    }
}
