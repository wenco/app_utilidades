using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboPruebaSeccionCreateModelValidator
        : AbstractValidator<DboPruebaSeccionCreateModel>
    {
        public DboPruebaSeccionCreateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Nombre).NotEmpty();
            RuleFor(p => p.Nombre).MaximumLength(255);
            #endregion
        }

    }
}
