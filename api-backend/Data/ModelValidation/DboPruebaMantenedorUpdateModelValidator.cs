using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboPruebaMantenedorUpdateModelValidator
        : AbstractValidator<DboPruebaMantenedorUpdateModel>
    {
        public DboPruebaMantenedorUpdateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Nombre).NotEmpty();
            RuleFor(p => p.Nombre).MaximumLength(255);
            RuleFor(p => p.Descripcion).MaximumLength(255);
            #endregion
        }

    }
}
