using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboAccionUpdateModelValidator
        : AbstractValidator<DboAccionUpdateModel>
    {
        public DboAccionUpdateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Controller).MaximumLength(50);
            RuleFor(p => p.Action).MaximumLength(50);
            RuleFor(p => p.Resource).MaximumLength(50);
            RuleFor(p => p.Caption).NotEmpty();
            RuleFor(p => p.Caption).MaximumLength(50);
            RuleFor(p => p.Descripcion).MaximumLength(500);
            #endregion
        }

    }
}
