using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboPeticionCreateModelValidator
        : AbstractValidator<DboPeticionCreateModel>
    {
        public DboPeticionCreateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Controller).NotEmpty();
            RuleFor(p => p.Controller).MaximumLength(64);
            RuleFor(p => p.Action).NotEmpty();
            RuleFor(p => p.Action).MaximumLength(64);
            RuleFor(p => p.Method).NotEmpty();
            RuleFor(p => p.Method).MaximumLength(16);
            #endregion
        }

    }
}
