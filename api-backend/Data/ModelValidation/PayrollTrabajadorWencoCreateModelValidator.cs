using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class PayrollTrabajadorWencoCreateModelValidator
        : AbstractValidator<PayrollTrabajadorWencoCreateModel>
    {
        public PayrollTrabajadorWencoCreateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.IdPayroll).NotEmpty();
            RuleFor(p => p.IdPayroll).MaximumLength(12);
            RuleFor(p => p.Nombre).NotEmpty();
            RuleFor(p => p.Nombre).MaximumLength(350);
            RuleFor(p => p.Nombres).MaximumLength(150);
            RuleFor(p => p.ApellidoPaterno).MaximumLength(150);
            RuleFor(p => p.ApellidoMaterno).MaximumLength(150);
            RuleFor(p => p.NombrePila).MaximumLength(150);
            RuleFor(p => p.Direccion).MaximumLength(128);
            RuleFor(p => p.Telefono).MaximumLength(18);
            RuleFor(p => p.CodigoProblemaHuella).MaximumLength(16);
            RuleFor(p => p.xClasif).MaximumLength(40);
            RuleFor(p => p.CodigoProblemaPuerta).MaximumLength(50);
            #endregion
        }

    }
}
