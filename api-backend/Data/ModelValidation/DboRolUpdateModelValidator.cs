using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboRolUpdateModelValidator
        : AbstractValidator<DboRolUpdateModel>
    {
        public DboRolUpdateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Nombre).NotEmpty();
            RuleFor(p => p.Nombre).MaximumLength(150);
            #endregion
        }

    }
}
