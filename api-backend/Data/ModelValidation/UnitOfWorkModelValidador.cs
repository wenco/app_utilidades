using System.Linq;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Models;
using FluentValidation.Results;

namespace api.Data.ModelValidation
{
    public static class UnitOfWorkModelValidador
    {
        #region Declarar
        private static DboUsuarioCreateModelValidator dboUsuarioCreateModelValidator = new DboUsuarioCreateModelValidator();
        private static DboUsuarioUpdateModelValidator dboUsuarioUpdateModelValidator = new DboUsuarioUpdateModelValidator();
        private static DboRolUsuarioAplicacionCreateModelValidator dboRolUsuarioAplicacionCreateModelValidator = new DboRolUsuarioAplicacionCreateModelValidator();
        private static DboRolUsuarioAplicacionUpdateModelValidator dboRolUsuarioAplicacionUpdateModelValidator = new DboRolUsuarioAplicacionUpdateModelValidator();
        private static DboRolCreateModelValidator dboRolCreateModelValidator = new DboRolCreateModelValidator();
        private static DboRolUpdateModelValidator dboRolUpdateModelValidator = new DboRolUpdateModelValidator();
        private static DboRolAreaAccionCreateModelValidator dboRolAreaAccionCreateModelValidator = new DboRolAreaAccionCreateModelValidator();
        private static DboRolAreaAccionUpdateModelValidator dboRolAreaAccionUpdateModelValidator = new DboRolAreaAccionUpdateModelValidator();
        private static DboPruebaSeccionCreateModelValidator dboPruebaSeccionCreateModelValidator = new DboPruebaSeccionCreateModelValidator();
        private static DboPruebaSeccionUpdateModelValidator dboPruebaSeccionUpdateModelValidator = new DboPruebaSeccionUpdateModelValidator();
        private static DboPruebaMantenedorCreateModelValidator dboPruebaMantenedorCreateModelValidator = new DboPruebaMantenedorCreateModelValidator();
        private static DboPruebaMantenedorUpdateModelValidator dboPruebaMantenedorUpdateModelValidator = new DboPruebaMantenedorUpdateModelValidator();
        private static DboPeticionCreateModelValidator dboPeticionCreateModelValidator = new DboPeticionCreateModelValidator();
        private static DboPeticionUpdateModelValidator dboPeticionUpdateModelValidator = new DboPeticionUpdateModelValidator();
        private static DboAplicacionCreateModelValidator dboAplicacionCreateModelValidator = new DboAplicacionCreateModelValidator();
        private static DboAplicacionUpdateModelValidator dboAplicacionUpdateModelValidator = new DboAplicacionUpdateModelValidator();
        private static DboAccionUsuarioCreateModelValidator dboAccionUsuarioCreateModelValidator = new DboAccionUsuarioCreateModelValidator();
        private static DboAccionUsuarioUpdateModelValidator dboAccionUsuarioUpdateModelValidator = new DboAccionUsuarioUpdateModelValidator();
        private static DboAccionPeticionCreateModelValidator dboAccionPeticionCreateModelValidator = new DboAccionPeticionCreateModelValidator();
        private static DboAccionPeticionUpdateModelValidator dboAccionPeticionUpdateModelValidator = new DboAccionPeticionUpdateModelValidator();
        private static DboAccionCreateModelValidator dboAccionCreateModelValidator = new DboAccionCreateModelValidator();
        private static DboAccionUpdateModelValidator dboAccionUpdateModelValidator = new DboAccionUpdateModelValidator();
        #endregion

        #region Validador
        public static ResponseRequestModel<DboUsuarioCreateModel> ValidateDboUsuarioCreateModelValidator(DboUsuarioCreateModel m)
        {
            ValidationResult results = dboUsuarioCreateModelValidator.Validate(m);
            return Validador<DboUsuarioCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboUsuarioUpdateModel> ValidateDboUsuarioUpdateModelValidator(DboUsuarioUpdateModel m)
        {
            ValidationResult results = dboUsuarioUpdateModelValidator.Validate(m);
            return Validador<DboUsuarioUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolUsuarioAplicacionCreateModel> ValidateDboRolUsuarioAplicacionCreateModelValidator(DboRolUsuarioAplicacionCreateModel m)
        {
            ValidationResult results = dboRolUsuarioAplicacionCreateModelValidator.Validate(m);
            return Validador<DboRolUsuarioAplicacionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolUsuarioAplicacionUpdateModel> ValidateDboRolUsuarioAplicacionUpdateModelValidator(DboRolUsuarioAplicacionUpdateModel m)
        {
            ValidationResult results = dboRolUsuarioAplicacionUpdateModelValidator.Validate(m);
            return Validador<DboRolUsuarioAplicacionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolCreateModel> ValidateDboRolCreateModelValidator(DboRolCreateModel m)
        {
            ValidationResult results = dboRolCreateModelValidator.Validate(m);
            return Validador<DboRolCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolUpdateModel> ValidateDboRolUpdateModelValidator(DboRolUpdateModel m)
        {
            ValidationResult results = dboRolUpdateModelValidator.Validate(m);
            return Validador<DboRolUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolAreaAccionCreateModel> ValidateDboRolAreaAccionCreateModelValidator(DboRolAreaAccionCreateModel m)
        {
            ValidationResult results = dboRolAreaAccionCreateModelValidator.Validate(m);
            return Validador<DboRolAreaAccionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboRolAreaAccionUpdateModel> ValidateDboRolAreaAccionUpdateModelValidator(DboRolAreaAccionUpdateModel m)
        {
            ValidationResult results = dboRolAreaAccionUpdateModelValidator.Validate(m);
            return Validador<DboRolAreaAccionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboPruebaSeccionCreateModel> ValidateDboPruebaSeccionCreateModelValidator(DboPruebaSeccionCreateModel m)
        {
            ValidationResult results = dboPruebaSeccionCreateModelValidator.Validate(m);
            return Validador<DboPruebaSeccionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboPruebaSeccionUpdateModel> ValidateDboPruebaSeccionUpdateModelValidator(DboPruebaSeccionUpdateModel m)
        {
            ValidationResult results = dboPruebaSeccionUpdateModelValidator.Validate(m);
            return Validador<DboPruebaSeccionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboPruebaMantenedorCreateModel> ValidateDboPruebaMantenedorCreateModelValidator(DboPruebaMantenedorCreateModel m)
        {
            ValidationResult results = dboPruebaMantenedorCreateModelValidator.Validate(m);
            return Validador<DboPruebaMantenedorCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboPruebaMantenedorUpdateModel> ValidateDboPruebaMantenedorUpdateModelValidator(DboPruebaMantenedorUpdateModel m)
        {
            ValidationResult results = dboPruebaMantenedorUpdateModelValidator.Validate(m);
            return Validador<DboPruebaMantenedorUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboPeticionCreateModel> ValidateDboPeticionCreateModelValidator(DboPeticionCreateModel m)
        {
            ValidationResult results = dboPeticionCreateModelValidator.Validate(m);
            return Validador<DboPeticionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboPeticionUpdateModel> ValidateDboPeticionUpdateModelValidator(DboPeticionUpdateModel m)
        {
            ValidationResult results = dboPeticionUpdateModelValidator.Validate(m);
            return Validador<DboPeticionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboAplicacionCreateModel> ValidateDboAplicacionCreateModelValidator(DboAplicacionCreateModel m)
        {
            ValidationResult results = dboAplicacionCreateModelValidator.Validate(m);
            return Validador<DboAplicacionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboAplicacionUpdateModel> ValidateDboAplicacionUpdateModelValidator(DboAplicacionUpdateModel m)
        {
            ValidationResult results = dboAplicacionUpdateModelValidator.Validate(m);
            return Validador<DboAplicacionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionUsuarioCreateModel> ValidateDboAccionUsuarioCreateModelValidator(DboAccionUsuarioCreateModel m)
        {
            ValidationResult results = dboAccionUsuarioCreateModelValidator.Validate(m);
            return Validador<DboAccionUsuarioCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionUsuarioUpdateModel> ValidateDboAccionUsuarioUpdateModelValidator(DboAccionUsuarioUpdateModel m)
        {
            ValidationResult results = dboAccionUsuarioUpdateModelValidator.Validate(m);
            return Validador<DboAccionUsuarioUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionPeticionCreateModel> ValidateDboAccionPeticionCreateModelValidator(DboAccionPeticionCreateModel m)
        {
            ValidationResult results = dboAccionPeticionCreateModelValidator.Validate(m);
            return Validador<DboAccionPeticionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionPeticionUpdateModel> ValidateDboAccionPeticionUpdateModelValidator(DboAccionPeticionUpdateModel m)
        {
            ValidationResult results = dboAccionPeticionUpdateModelValidator.Validate(m);
            return Validador<DboAccionPeticionUpdateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionCreateModel> ValidateDboAccionCreateModelValidator(DboAccionCreateModel m)
        {
            ValidationResult results = dboAccionCreateModelValidator.Validate(m);
            return Validador<DboAccionCreateModel>(results, m);
        }
        public static ResponseRequestModel<DboAccionUpdateModel> ValidateDboAccionUpdateModelValidator(DboAccionUpdateModel m)
        {
            ValidationResult results = dboAccionUpdateModelValidator.Validate(m);
            return Validador<DboAccionUpdateModel>(results, m);
        }
        #endregion
        #region Helper
        private static ResponseRequestModel<M> Validador<M>(ValidationResult results, M model)
        {
            if (!results.IsValid)
            {
                return new ResponseRequestModel<M>()
                {
                    Text = results.Errors.Select(x => x.ErrorMessage).ToList()
                        .Aggregate((i, j) => i + ", " + j),
                    ResponseRequest = ResponseRequestEnum.Warning
                };
            }
            else
            {
                return new ResponseRequestModel<M>()
                {
                    ResponseRequest = ResponseRequestEnum.Success
                };
            }
        }
        #endregion
    }
}
