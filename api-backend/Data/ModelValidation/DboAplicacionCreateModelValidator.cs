using System;
using FluentValidation;
using api.Data.Models;

namespace api.Data.ModelValidation
{
    public partial class DboAplicacionCreateModelValidator
        : AbstractValidator<DboAplicacionCreateModel>
    {
        public DboAplicacionCreateModelValidator()
        {
            #region Generated Constructor
            RuleFor(p => p.Alias).NotEmpty();
            RuleFor(p => p.Alias).MaximumLength(20);
            RuleFor(p => p.Titulo).NotEmpty();
            RuleFor(p => p.Titulo).MaximumLength(100);
            RuleFor(p => p.Descripcion).MaximumLength(1000);
            RuleFor(p => p.Imagen).MaximumLength(255);
            RuleFor(p => p.NgVersion).MaximumLength(10);
            #endregion
        }

    }
}
