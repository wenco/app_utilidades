using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace api.Data
{
    public partial class ContextoDb : DbContext
    {
        public ContextoDb(DbContextOptions<ContextoDb> options)
            : base(options)
        {
        }

        #region Generated Properties
        public virtual DbSet<api.Data.Entities.DboAccionEntity> DboAccionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboAccionPeticionEntity> DboAccionPeticionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboAccionUsuarioEntity> DboAccionUsuarioEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboAplicacionEntity> DboAplicacionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboEmpresaEntity> DboEmpresaEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboPeticionEntity> DboPeticionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboRolAreaAccionEntity> DboRolAreaAccionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboRolEntity> DboRolEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboRolUsuarioAplicacionEntity> DboRolUsuarioAplicacionEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.DboUsuarioEntity> DboUsuarioEntityDataSet { get; set; }

        public virtual DbSet<api.Data.Entities.PayrollTrabajadorWencoEntity> PayrollTrabajadorWencoEntityDataSet { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Generated Configuration
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboAccionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboAccionPeticionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboAccionUsuarioEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboAplicacionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboEmpresaEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboPeticionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboRolAreaAccionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboRolEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboRolUsuarioAplicacionEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.DboUsuarioEntityMap());
            modelBuilder.ApplyConfiguration(new api.Data.Mapping.PayrollTrabajadorWencoEntityMap());
            #endregion
        }
    }
}
