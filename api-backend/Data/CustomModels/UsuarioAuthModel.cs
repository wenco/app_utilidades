using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using api.Data.Models;

namespace api.Data.CustomModels
{
    public class UsuarioAuthModel
    {
        public int IdUsuario { get; set; }

        public string Token { get; set; }

        public DateTime? FechaExpiracion { get; set; }

        public string TokenPublico { get; set; }

        public bool? RequiereHuellaInactividad { get; set; }
        public List<DboRolUsuarioAplicacionReadModel> dboRolUsuarioAplicacionReadModelList { get; set; }
        public List<DboAccionReadModel> MenuList { get; set; }
        public DboAplicacionReadModel dboAplicacionReadModel { get; set; }
        public DboEmpresaReadModel dboEmpresaReadActual { get; set; }
        public string AdSamAccountName { get; set; }
        public string PassWord { get; set; }

    }
}