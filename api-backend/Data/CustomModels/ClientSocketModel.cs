using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data.Entities;
using api.Data.Models;

namespace api.Data.CustomModels
{
    public class ClientSocketModel : ICloneable
    {
        public string ConnectionId { get; set; }
        public DboUsuarioReadModel DboUsuario { get; set; }
        public DateTime FechaUltimoMsg { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}