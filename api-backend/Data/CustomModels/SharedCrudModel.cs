using api.Data.CustomEnums;

namespace api.Data.CustomModels
{
    public class SharedCrud<T>
    {
        public CrudEnum Crud { get; set; }
        public T Data { get; set; }
    }
}