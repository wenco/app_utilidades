using api.Data.CustomEnums;

namespace api.Data.CustomModels
{
    public class ResponseRequestModel<T>
    {
        public string Text { get; set; }
        public ResponseRequestEnum ResponseRequest { get; set; }
        public bool Hidden { get; set; } = false;
        public T Data { get; set; }
    }
}