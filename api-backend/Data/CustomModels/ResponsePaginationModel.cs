using System;
using System.Collections.Generic;
using AutoMapper;

namespace api.Data.CustomModels
{
    public class ResponsePaginationModel<T>
    {
        public int TotalData { get; set; }
        public List<T> Data { get; set; }
    }
}