using api.Data.CustomEnums;

namespace api.Data.CustomModels
{
    public class ErrorModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
    }
}