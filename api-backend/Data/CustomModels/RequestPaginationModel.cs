using System;
using System.Collections.Generic;
using api.Data.Entities;
using AutoMapper;

namespace api.Data.CustomModels
{
    public class RequestPaginationModel<T>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public T Filter { get; set; }
        // public DboUsuarioEntity DboUsuario { get; set; }
    }
}