using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class PayrollTrabajadorWencoUpdateModel
    {
        #region Generated Properties
        public int IdTrabajadorWenco { get; set; }

        public string IdPayroll { get; set; }

        public string Nombre { get; set; }

        public int? IdCentroCosto { get; set; }

        public int? IdJefeTrabajador { get; set; }

        public int EsComiteParitario { get; set; }

        public int? Activo { get; set; }

        public int? Categoria { get; set; }

        public string Nombres { get; set; }

        public string ApellidoPaterno { get; set; }

        public string ApellidoMaterno { get; set; }

        public string NombrePila { get; set; }

        public int? Cargo { get; set; }

        public int? IdEmpresa { get; set; }

        public DateTime? FechaNacimiento { get; set; }

        public DateTime? FechaIngreso { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public int? MarcaAsistencia { get; set; }

        public int? PermiteHhee { get; set; }

        public string CodigoProblemaHuella { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public DateTime? FechaEdicion { get; set; }

        public long? FechaEdicionEpoch { get; set; }

        public int? Clasif { get; set; }

        public string xClasif { get; set; }

        public bool? Confidencial { get; set; }

        public string CodigoProblemaPuerta { get; set; }

        #endregion

    }
}
