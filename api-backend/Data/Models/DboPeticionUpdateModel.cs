using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboPeticionUpdateModel
    {
        #region Generated Properties
        public int IdPeticion { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Method { get; set; }

        public int IdAplicacion { get; set; }

        public bool? Restriccion { get; set; }

        #endregion

    }
}
