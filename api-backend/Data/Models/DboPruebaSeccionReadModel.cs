using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboPruebaSeccionReadModel
    {
        #region Generated Properties
        public int IdPruebaSeccion { get; set; }

        public string Nombre { get; set; }

        #endregion

    }
}
