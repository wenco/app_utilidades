using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboPruebaMantenedorReadModel
    {
        #region Generated Properties
        public int IdPruebaMantenedor { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int Activo { get; set; }

        public int? IdPruebaSeccion { get; set; }

        public int? IdPruebaSeccion2 { get; set; }

        public int? Cantidad { get; set; }

        public int? Cantidad2 { get; set; }

        #endregion
        public virtual DboPruebaSeccionReadModel FkPruebaMantenedorPruebaSeccion { get; set; }

    }
}
