using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboAccionReadModel
    {
        #region Generated Properties
        public int IdAccion { get; set; }

        public int IdAplicacion { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Resource { get; set; }

        public string Caption { get; set; }

        public string Descripcion { get; set; }

        public api.Data.CustomEnums.YesNoEnum? Activo { get; set; }

        public api.Data.CustomEnums.TipoAccionEnum? Tipo { get; set; }

        public int? IdAccionSuper { get; set; }

        public int? Orden { get; set; }

        public int? Seleccionable { get; set; }

        #endregion
        public virtual DboAccionReadModel FkAccionAccionSuper { get; set; }
        public virtual List<DboAccionReadModel> FkAccionAccionSuperList { get; set; }
        public int Nivel { get; set; }
        public bool Seleccionado { get; set; }
        public bool IsOpen { get; set; }
        public bool IsHome { get; set; }
    }
}
