using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboUsuarioReadModel
    {
        #region Generated Properties
        public int IdUsuario { get; set; }

        public int IdTrabajadorWenco { get; set; }

        public int? IdEmpresaLogin { get; set; }

        public string AdSamAccountName { get; set; }

        public string Email { get; set; }

        public int? IdArea { get; set; }

        public api.Data.CustomEnums.YesNoEnum? Activo { get; set; }

        public int? SocIdPerfilEspecial { get; set; }

        public DateTime? FechaExpiracion { get; set; }

        public bool? RequiereHuellaInactividad { get; set; }

        #endregion

    }
}
