using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboAccionUsuarioReadModel
    {
        #region Generated Properties
        public int IdAccionUsuario { get; set; }

        public int IdAccion { get; set; }

        public int IdUsuario { get; set; }

        public api.Data.CustomEnums.YesNoEnum TienePermiso { get; set; }

        #endregion

    }
}
