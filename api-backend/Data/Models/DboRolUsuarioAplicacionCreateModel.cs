using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboRolUsuarioAplicacionCreateModel
    {
        #region Generated Properties
        public int IdRol { get; set; }

        public int IdUsuario { get; set; }

        public int IdAplicacion { get; set; }

        public int? CantidadAccesoUsuario { get; set; }

        #endregion

    }
}
