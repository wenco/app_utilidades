using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboAccionPeticionReadModel
    {
        #region Generated Properties
        public int IdAccionPeticion { get; set; }

        public int IdAccion { get; set; }

        public int IdPeticion { get; set; }

        #endregion

    }
}
