using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboRolAreaAccionReadModel
    {
        #region Generated Properties
        public int IdRol { get; set; }

        public int IdArea { get; set; }

        public int IdAccion { get; set; }

        #endregion

    }
}
