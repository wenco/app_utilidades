using System;
using System.Collections.Generic;

namespace api.Data.Models
{
    public partial class DboRolUpdateModel
    {
        #region Generated Properties
        public int IdRol { get; set; }

        public string Nombre { get; set; }

        #endregion

    }
}
