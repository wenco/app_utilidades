using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboEmpresaEntity
    {
        public DboEmpresaEntity()
        {
            #region Generated Constructor
            FkUsuarioEmpresaList = new HashSet<DboUsuarioEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdEmpresa { get; set; }

        public string Descripcion { get; set; }

        public string ConnectionString { get; set; }

        public string Dominio { get; set; }

        public string Nombre { get; set; }

        public string IpServidorNotificacion { get; set; }

        public string PuertoServidorNotificacion { get; set; }

        public string IpServidorRasSincronizador { get; set; }

        public string BaseDatosPayroll { get; set; }

        public string PuertoServidorRasSincronizador { get; set; }

        public string ServidorSistemas { get; set; }

        public string DbSistemas { get; set; }

        public string UrlBackoffice { get; set; }

        public string Nacionalidad { get; set; }

        public string RazonSocial { get; set; }

        public int? IdEmpresaTrabajador { get; set; }

        public string Rut { get; set; }

        public string Direccion { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public DateTime? FechaEdicion { get; set; }

        public long? FechaEdicionEpoch { get; set; }

        public string LinkedServer { get; set; }

        public int? IdEmpresaCg { get; set; }

        public string NombrePublicador { get; set; }

        public int? EmpresaActual { get; set; }

        public string LinkedServerErp { get; set; }

        public string DbErp { get; set; }

        public int? IdEmpresaTrabajadorChile { get; set; }

        public int? IdEmpresaTrabajadorPeru { get; set; }

        public int? TipoSwRrhh { get; set; }

        public string LinkedServerSwRrhh { get; set; }

        public string DbSwRrhh { get; set; }

        public string Giro { get; set; }

        public string UrlBackend { get; set; }

        public int? GpProduccion { get; set; }

        public string CorreoDte { get; set; }

        public string EmpresaDte { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<DboUsuarioEntity> FkUsuarioEmpresaList { get; set; }

        #endregion

    }
}
