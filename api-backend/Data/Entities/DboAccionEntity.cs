using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboAccionEntity
    {
        public DboAccionEntity()
        {
            #region Generated Constructor
            FkAccionAccionSuperList = new HashSet<DboAccionEntity>();
            FkAccionPeticionAccionList = new HashSet<DboAccionPeticionEntity>();
            FkAccionUsuarioAccionList = new HashSet<DboAccionUsuarioEntity>();
            FkRolAreaAccionAccionList = new HashSet<DboRolAreaAccionEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdAccion { get; set; }

        public int IdAplicacion { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Resource { get; set; }

        public string Caption { get; set; }

        public string Descripcion { get; set; }

        public api.Data.CustomEnums.YesNoEnum? Activo { get; set; }

        public api.Data.CustomEnums.TipoAccionEnum? Tipo { get; set; }

        public int? IdAccionSuper { get; set; }

        public int? Orden { get; set; }

        public int? Seleccionable { get; set; }

        #endregion

        #region Generated Relationships
        public virtual DboAccionEntity FkAccionAccionSuper { get; set; }

        public virtual ICollection<DboAccionEntity> FkAccionAccionSuperList { get; set; }

        public virtual DboAplicacionEntity FkAccionAplicacion { get; set; }

        public virtual ICollection<DboAccionPeticionEntity> FkAccionPeticionAccionList { get; set; }

        public virtual ICollection<DboAccionUsuarioEntity> FkAccionUsuarioAccionList { get; set; }

        public virtual ICollection<DboRolAreaAccionEntity> FkRolAreaAccionAccionList { get; set; }

        #endregion

    }
}
