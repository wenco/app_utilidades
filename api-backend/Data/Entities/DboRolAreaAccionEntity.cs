using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboRolAreaAccionEntity
    {
        public DboRolAreaAccionEntity()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int IdRol { get; set; }

        public int IdArea { get; set; }

        public int IdAccion { get; set; }

        #endregion

        #region Generated Relationships
        public virtual DboAccionEntity FkRolAreaAccionAccion { get; set; }

        public virtual DboRolEntity FkRolAreaAccionRol { get; set; }

        #endregion

    }
}
