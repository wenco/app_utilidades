using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboRolEntity
    {
        public DboRolEntity()
        {
            #region Generated Constructor
            FkRolAreaAccionRolList = new HashSet<DboRolAreaAccionEntity>();
            FkRolUsuarioAplicacionRolList = new HashSet<DboRolUsuarioAplicacionEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdRol { get; set; }

        public string Nombre { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<DboRolAreaAccionEntity> FkRolAreaAccionRolList { get; set; }

        public virtual ICollection<DboRolUsuarioAplicacionEntity> FkRolUsuarioAplicacionRolList { get; set; }

        #endregion

    }
}
