using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboUsuarioEntity
    {
        public DboUsuarioEntity()
        {
            #region Generated Constructor
            FkAccionUsuarioUsuarioList = new HashSet<DboAccionUsuarioEntity>();
            FkRolUsuarioAplicacionUsuarioList = new HashSet<DboRolUsuarioAplicacionEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdUsuario { get; set; }

        public int IdTrabajadorWenco { get; set; }

        public int? IdEmpresaLogin { get; set; }

        public string AdSamAccountName { get; set; }

        public string Email { get; set; }

        public int? IdArea { get; set; }

        public api.Data.CustomEnums.YesNoEnum? Activo { get; set; }

        public int? SocIdPerfilEspecial { get; set; }

        public string Token { get; set; }

        public DateTime? FechaExpiracion { get; set; }

        public string TokenPublico { get; set; }

        public bool? RequiereHuellaInactividad { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<DboAccionUsuarioEntity> FkAccionUsuarioUsuarioList { get; set; }

        public virtual ICollection<DboRolUsuarioAplicacionEntity> FkRolUsuarioAplicacionUsuarioList { get; set; }

        public virtual DboEmpresaEntity FkUsuarioEmpresa { get; set; }

        public virtual PayrollTrabajadorWencoEntity FkUsuarioTrabajadorWenco { get; set; }

        #endregion

    }
}
