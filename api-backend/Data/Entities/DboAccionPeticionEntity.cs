using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboAccionPeticionEntity
    {
        public DboAccionPeticionEntity()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int IdAccionPeticion { get; set; }

        public int IdAccion { get; set; }

        public int IdPeticion { get; set; }

        #endregion

        #region Generated Relationships
        public virtual DboAccionEntity FkAccionPeticionAccion { get; set; }

        public virtual DboPeticionEntity FkAccionPeticionPeticion { get; set; }

        #endregion

    }
}
