using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboPeticionEntity
    {
        public DboPeticionEntity()
        {
            #region Generated Constructor
            FkAccionPeticionPeticionList = new HashSet<DboAccionPeticionEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdPeticion { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Method { get; set; }

        public int IdAplicacion { get; set; }

        public bool? Restriccion { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<DboAccionPeticionEntity> FkAccionPeticionPeticionList { get; set; }

        public virtual DboAplicacionEntity FkPeticionAplicacion { get; set; }

        #endregion

    }
}
