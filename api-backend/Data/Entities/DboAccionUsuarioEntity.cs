using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboAccionUsuarioEntity
    {
        public DboAccionUsuarioEntity()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int IdAccionUsuario { get; set; }

        public int IdAccion { get; set; }

        public int IdUsuario { get; set; }

        public api.Data.CustomEnums.YesNoEnum TienePermiso { get; set; }

        #endregion

        #region Generated Relationships
        public virtual DboAccionEntity FkAccionUsuarioAccion { get; set; }

        public virtual DboUsuarioEntity FkAccionUsuarioUsuario { get; set; }

        #endregion

    }
}
