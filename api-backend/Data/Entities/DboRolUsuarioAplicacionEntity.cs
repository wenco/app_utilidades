using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboRolUsuarioAplicacionEntity
    {
        public DboRolUsuarioAplicacionEntity()
        {
            #region Generated Constructor
            #endregion
        }

        #region Generated Properties
        public int IdRol { get; set; }

        public int IdUsuario { get; set; }

        public int IdAplicacion { get; set; }

        public int? CantidadAccesoUsuario { get; set; }

        #endregion

        #region Generated Relationships
        public virtual DboAplicacionEntity FkRolUsuarioAplicacionAplicacion { get; set; }

        public virtual DboRolEntity FkRolUsuarioAplicacionRol { get; set; }

        public virtual DboUsuarioEntity FkRolUsuarioAplicacionUsuario { get; set; }

        #endregion

    }
}
