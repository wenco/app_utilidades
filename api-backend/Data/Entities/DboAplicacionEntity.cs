using System;
using System.Collections.Generic;

namespace api.Data.Entities
{
    public partial class DboAplicacionEntity
    {
        public DboAplicacionEntity()
        {
            #region Generated Constructor
            FkAccionAplicacionList = new HashSet<DboAccionEntity>();
            FkPeticionAplicacionList = new HashSet<DboPeticionEntity>();
            FkRolUsuarioAplicacionAplicacionList = new HashSet<DboRolUsuarioAplicacionEntity>();
            #endregion
        }

        #region Generated Properties
        public int IdAplicacion { get; set; }

        public string Alias { get; set; }

        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public string Imagen { get; set; }

        public int? Visible { get; set; }

        public int Arquitectura { get; set; }

        public string BaseApi { get; set; }

        public string RutaDistFrontend { get; set; }

        public string RutaDistBackend { get; set; }

        public string BackendAppPool { get; set; }

        public string FrontendAppPool { get; set; }

        public string ComputerName { get; set; }

        public int? EsMovil { get; set; }

        public string NgVersion { get; set; }

        public string RutaRespaldoFrontend { get; set; }

        public string RutaRespaldoBackend { get; set; }

        #endregion

        #region Generated Relationships
        public virtual ICollection<DboAccionEntity> FkAccionAplicacionList { get; set; }

        public virtual ICollection<DboPeticionEntity> FkPeticionAplicacionList { get; set; }

        public virtual ICollection<DboRolUsuarioAplicacionEntity> FkRolUsuarioAplicacionAplicacionList { get; set; }

        #endregion

    }
}
