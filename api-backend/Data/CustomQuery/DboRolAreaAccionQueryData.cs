using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboRolAreaAccionQueryData
    {
        #region No Modificar
        public DboRolAreaAccionQueryEnum TipoFiltro { get; set; }
        #endregion
        public int IdRol { get; internal set; }
        public int IdArea { get; internal set; }
        public int IdAplicacion { get; set; }
    }
}

