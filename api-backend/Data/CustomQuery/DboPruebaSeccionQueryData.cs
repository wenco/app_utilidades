using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboPruebaSeccionQueryData
    {
        #region No Modificar
        public DboPruebaSeccionQueryEnum TipoFiltro { get; set; }
        public string filtro { get; set; }
        #endregion
        public int? IdPruebaSeccion { get; set; }
    }
}

