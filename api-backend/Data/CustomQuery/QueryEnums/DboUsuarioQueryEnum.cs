using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboUsuarioQueryEnum
    {
        SinIncludeSinFiltro = 1,
        BusquedaPorTokenPublico = 10,
        BusquedaPorIdUsuario = 11,
        BusquedaPorAdSamAccountName = 12
    }
}

