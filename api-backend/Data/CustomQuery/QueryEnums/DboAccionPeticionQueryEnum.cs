using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboAccionPeticionQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeBusquedaPorAccionPeticion = 10
    }
}

