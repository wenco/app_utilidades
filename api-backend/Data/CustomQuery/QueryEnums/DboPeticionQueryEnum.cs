using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboPeticionQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeBusquedaPeticion = 10,
        SinIncludeBusquedaPorAplicacion = 11
    }
}

