using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboPruebaMantenedorQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeConFiltro = 2,
        ConIncludeSeccionGetByIdMantenedor = 10
    }
}

