using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboAplicacionQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeBusquedaPorAliasApp = 10
    }
}

