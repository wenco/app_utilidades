using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboAccionUsuarioQueryEnum
    {
        SinIncludeSinFiltro = 1,         
        SinIncludeConFiltro = 2,
        BuscarPorUsuarioAplicacion = 10
    }
}

