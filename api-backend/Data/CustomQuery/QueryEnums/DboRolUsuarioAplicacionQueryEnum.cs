using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboRolUsuarioAplicacionQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeConFiltro = 2,
        BuscarPorUsuarioAplicacion = 10
    }
}

