using System;

namespace api.Data.CustomQuery.QueryEnum
{
    public enum DboPruebaSeccionQueryEnum
    {
        SinIncludeSinFiltro = 1,
        SinIncludeConFiltro = 2,
        SinIncludeConFiltroYSecccion = 3
    }
}

