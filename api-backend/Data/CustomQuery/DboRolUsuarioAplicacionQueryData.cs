using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboRolUsuarioAplicacionQueryData
    {
        #region No Modificar
        public DboRolUsuarioAplicacionQueryEnum TipoFiltro { get; set; }
        public string Filtro { get; set; }
        #endregion
        public string AliasApp { get; set; }
        public int IdUsuario { get; set; }
    }
}

