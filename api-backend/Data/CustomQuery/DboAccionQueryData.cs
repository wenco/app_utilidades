using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboAccionQueryData
    {
        #region No Modificar
        public DboAccionQueryEnum TipoFiltro { get; set; }
        #endregion
        public int IdAccion { get; set; }
    }
}

