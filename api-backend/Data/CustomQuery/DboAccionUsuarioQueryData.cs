using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboAccionUsuarioQueryData
    {
        #region No Modificar
        public DboAccionUsuarioQueryEnum TipoFiltro { get; set; }
        #endregion
        public int IdUsuario { get; internal set; }
        public int IdAplicacion { get; internal set; }
    }
}

