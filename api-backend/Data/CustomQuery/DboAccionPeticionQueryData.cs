using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboAccionPeticionQueryData
    {
        #region No Modificar
        public DboAccionPeticionQueryEnum TipoFiltro { get; set; }
        #endregion
        public int IdAccion { get; set; }
        public int IdPeticion { get; set; }
    }
}

