using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboAplicacionQueryData
    {
        #region No Modificar
        public DboAplicacionQueryEnum TipoFiltro { get; set; }
        #endregion
        public string aliasApp { get; set; }
    }
}

