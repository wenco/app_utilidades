using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboPeticionQueryData
    {
        #region No Modificar
        public DboPeticionQueryEnum TipoFiltro { get; set; }
        #endregion
        public string Controlador { get; set; }
        public string MetodoAccion { get; set; }
        public string MetodoHttp { get; set; }
        public string aliasApp { get; set; }
    }
}

