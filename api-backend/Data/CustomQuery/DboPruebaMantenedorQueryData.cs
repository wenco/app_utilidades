using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboPruebaMantenedorQueryData
    {
        #region No Modificar
        public DboPruebaMantenedorQueryEnum TipoFiltro { get; set; }
        public string Filtro { get; set; }
        #endregion
        public int IdPruebaMantenedor { get; set; }
    }
}

