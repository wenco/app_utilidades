using System;
using api.Data.CustomQuery.QueryEnum;

namespace api.Data.CustomQuery
{
    public class DboUsuarioQueryData
    {
        #region No Modificar
        public DboUsuarioQueryEnum TipoFiltro { get; set; }
        public string filtro { get; set; }
        #endregion
        public string TokenPublico { get; internal set; }
        public int IdUsuario { get; internal set; }
        public string AdSamAccountName { get; set; }
    }
}

