using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace api.Middleware
{
    public class ElmahMiddleware
    {
        private RequestDelegate requestDelegate;
        public ElmahMiddleware(RequestDelegate requestDelegate)
        {
            this.requestDelegate = requestDelegate;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.HasValue && context.Request.Path.Value == @"/elmah/stylesheet")
            {
                context.Response.Redirect("/Static/elmah.min.css");
            }
            else
            {
                await this.requestDelegate(context);
            }
        }
    }
}