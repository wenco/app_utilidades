using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboAplicacionService
    {
        #region No Modificar
        ResponsePaginationModel<DboAplicacionEntity> GetPagination(RequestPaginationModel<DboAplicacionQueryData> requestPagination);
        List<DboAplicacionEntity> GetList(DboAplicacionQueryData queryData);
        DboAplicacionEntity GetOne(DboAplicacionQueryData queryData);
        ResponseRequestModel<DboAplicacionEntity> Delete(DboAplicacionEntity DboAplicacionEntity);
        ResponseRequestModel<DboAplicacionEntity> Create(DboAplicacionEntity DboAplicacionEntity);
        ResponseRequestModel<DboAplicacionEntity> Update(DboAplicacionEntity DboAplicacionEntity, DboAplicacionUpdateModel dboAplicacionUpdateModel);
        #endregion
    }
}

