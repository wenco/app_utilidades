using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboAccionPeticionService
    {
        #region No Modificar
        ResponsePaginationModel<DboAccionPeticionEntity> GetPagination(RequestPaginationModel<DboAccionPeticionQueryData> requestPagination);
        List<DboAccionPeticionEntity> GetList(DboAccionPeticionQueryData queryData);
        DboAccionPeticionEntity GetOne(DboAccionPeticionQueryData queryData);
        ResponseRequestModel<DboAccionPeticionEntity> Delete(DboAccionPeticionEntity DboAccionPeticionEntity);
        ResponseRequestModel<DboAccionPeticionEntity> Create(DboAccionPeticionEntity DboAccionPeticionEntity);
        ResponseRequestModel<DboAccionPeticionEntity> Update(DboAccionPeticionEntity DboAccionPeticionEntity, DboAccionPeticionUpdateModel dboAccionPeticionUpdateModel);
        #endregion
    }
}

