using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboRolUsuarioAplicacionService
    {
        #region No Modificar
        ResponsePaginationModel<DboRolUsuarioAplicacionEntity> GetPagination(RequestPaginationModel<DboRolUsuarioAplicacionQueryData> requestPagination);
        List<DboRolUsuarioAplicacionEntity> GetList(DboRolUsuarioAplicacionQueryData queryData);
        DboRolUsuarioAplicacionEntity GetOne(DboRolUsuarioAplicacionQueryData queryData);
        ResponseRequestModel<DboRolUsuarioAplicacionEntity> Delete(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity);
        ResponseRequestModel<DboRolUsuarioAplicacionEntity> Create(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity);
        ResponseRequestModel<DboRolUsuarioAplicacionEntity> Update(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity, DboRolUsuarioAplicacionUpdateModel dboRolUsuarioAplicacionUpdateModel);
        #endregion
    }
}

