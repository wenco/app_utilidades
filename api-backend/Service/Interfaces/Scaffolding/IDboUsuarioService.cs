using System;
using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboUsuarioService
    {
        #region No Modificar
        ResponsePaginationModel<DboUsuarioEntity> GetPagination(RequestPaginationModel<DboUsuarioQueryData> requestPagination);
        List<DboUsuarioEntity> GetList(DboUsuarioQueryData queryData);
        DboUsuarioEntity GetOne(DboUsuarioQueryData queryData);
        ResponseRequestModel<DboUsuarioEntity> Delete(DboUsuarioEntity DboUsuarioEntity);
        ResponseRequestModel<DboUsuarioEntity> Create(DboUsuarioEntity DboUsuarioEntity);
        ResponseRequestModel<DboUsuarioEntity> Update(DboUsuarioEntity DboUsuarioEntity);
        ResponseRequestModel<DboUsuarioEntity> UpdateFromModel(DboUsuarioEntity DboUsuarioEntity, DboUsuarioUpdateModel dboUsuarioUpdateModel);
        #endregion
        DboUsuarioEntity AsignarLoginData(SettingsConfiguration settingsConfiguration, int idUsuario, string Token = null, DateTime? fechaExpiracion = null);
    }
}

