using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboAccionService
    {
        #region No Modificar
        ResponsePaginationModel<DboAccionEntity> GetPagination(RequestPaginationModel<DboAccionQueryData> requestPagination);
        List<DboAccionEntity> GetList(DboAccionQueryData queryData);
        DboAccionEntity GetOne(DboAccionQueryData queryData);
        ResponseRequestModel<DboAccionEntity> Delete(DboAccionEntity DboAccionEntity);
        ResponseRequestModel<DboAccionEntity> Create(DboAccionEntity DboAccionEntity);
        ResponseRequestModel<DboAccionEntity> Update(DboAccionEntity DboAccionEntity, DboAccionUpdateModel dboAccionUpdateModel);
        #endregion
    }
}

