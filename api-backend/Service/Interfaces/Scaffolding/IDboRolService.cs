using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboRolService
    {
        #region No Modificar
        ResponsePaginationModel<DboRolEntity> GetPagination(RequestPaginationModel<DboRolQueryData> requestPagination);
        List<DboRolEntity> GetList(DboRolQueryData queryData);
        DboRolEntity GetOne(DboRolQueryData queryData);
        ResponseRequestModel<DboRolEntity> Delete(DboRolEntity DboRolEntity);
        ResponseRequestModel<DboRolEntity> Create(DboRolEntity DboRolEntity);
        ResponseRequestModel<DboRolEntity> Update(DboRolEntity DboRolEntity, DboRolUpdateModel dboRolUpdateModel);
        #endregion
    }
}

