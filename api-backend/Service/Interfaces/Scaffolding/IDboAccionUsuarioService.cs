using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboAccionUsuarioService
    {
        #region No Modificar
        ResponsePaginationModel<DboAccionUsuarioEntity> GetPagination(RequestPaginationModel<DboAccionUsuarioQueryData> requestPagination);
        List<DboAccionUsuarioEntity> GetList(DboAccionUsuarioQueryData queryData);
        DboAccionUsuarioEntity GetOne(DboAccionUsuarioQueryData queryData);
        ResponseRequestModel<DboAccionUsuarioEntity> Delete(DboAccionUsuarioEntity DboAccionUsuarioEntity);
        ResponseRequestModel<DboAccionUsuarioEntity> Create(DboAccionUsuarioEntity DboAccionUsuarioEntity);
        ResponseRequestModel<DboAccionUsuarioEntity> Update(DboAccionUsuarioEntity DboAccionUsuarioEntity, DboAccionUsuarioUpdateModel dboAccionUsuarioUpdateModel);
        #endregion
    }
}

