using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboPeticionService
    {
        #region No Modificar
        ResponsePaginationModel<DboPeticionEntity> GetPagination(RequestPaginationModel<DboPeticionQueryData> requestPagination);
        List<DboPeticionEntity> GetList(DboPeticionQueryData queryData);
        DboPeticionEntity GetOne(DboPeticionQueryData queryData);
        ResponseRequestModel<DboPeticionEntity> Delete(DboPeticionEntity DboPeticionEntity);
        ResponseRequestModel<DboPeticionEntity> Create(DboPeticionEntity DboPeticionEntity);
        ResponseRequestModel<DboPeticionEntity> Update(DboPeticionEntity DboPeticionEntity, DboPeticionUpdateModel dboPeticionUpdateModel);
        #endregion
        void CrearPeticiones(List<DboPeticionEntity> dboPeticionList, SettingsConfiguration settingsConfiguration);
    }
}

