using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboRolAreaAccionService
    {
        #region No Modificar
        ResponsePaginationModel<DboRolAreaAccionEntity> GetPagination(RequestPaginationModel<DboRolAreaAccionQueryData> requestPagination);
        List<DboRolAreaAccionEntity> GetList(DboRolAreaAccionQueryData queryData);
        DboRolAreaAccionEntity GetOne(DboRolAreaAccionQueryData queryData);
        ResponseRequestModel<DboRolAreaAccionEntity> Delete(DboRolAreaAccionEntity DboRolAreaAccionEntity);
        ResponseRequestModel<DboRolAreaAccionEntity> Create(DboRolAreaAccionEntity DboRolAreaAccionEntity);
        ResponseRequestModel<DboRolAreaAccionEntity> Update(DboRolAreaAccionEntity DboRolAreaAccionEntity, DboRolAreaAccionUpdateModel dboRolAreaAccionUpdateModel);
        #endregion
    }
}

