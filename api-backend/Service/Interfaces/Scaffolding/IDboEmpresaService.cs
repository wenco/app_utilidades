using System.Collections.Generic;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Scaffolding
{
    public interface IDboEmpresaService
    {
        #region No Modificar
        ResponsePaginationModel<DboEmpresaEntity> GetPagination(RequestPaginationModel<DboEmpresaQueryData> requestPagination);
        List<DboEmpresaEntity> GetList(DboEmpresaQueryData queryData);
        DboEmpresaEntity GetOne(DboEmpresaQueryData queryData);
        void Reload(DboEmpresaEntity dboEmpresa);
        ResponseRequestModel<DboEmpresaEntity> Delete(DboEmpresaEntity DboEmpresaEntity);
        ResponseRequestModel<DboEmpresaEntity> Create(DboEmpresaEntity DboEmpresaEntity);
        ResponseRequestModel<DboEmpresaEntity> Update(DboEmpresaEntity DboEmpresaEntity, DboEmpresaUpdateModel dboEmpresaUpdateModel);
        #endregion
    }
}

