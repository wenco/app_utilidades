using api.Service.Interfaces.Scaffolding;

namespace api.Service.Interfaces.Base
{
    public interface IUnitOfWorkService
    {
        #region Declarar
        IDboUsuarioService dboUsuarioService { get; }
        IDboRolUsuarioAplicacionService dboRolUsuarioAplicacionService { get; }
        IDboRolService dboRolService { get; }
        IDboRolAreaAccionService dboRolAreaAccionService { get; }
        IDboPeticionService dboPeticionService { get; }
        IDboAplicacionService dboAplicacionService { get; }
        IDboAccionUsuarioService dboAccionUsuarioService { get; }
        IDboAccionPeticionService dboAccionPeticionService { get; }
        IDboAccionService dboAccionService { get; }
        IDboEmpresaService dboEmpresaService { get; }
        #endregion
        IAuthService authService { get; }
    }
}
