using System.Collections.Generic;
using System.Security.Claims;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;

namespace api.Service.Interfaces.Base
{
    public interface IAuthService
    {
        DboRolUsuarioAplicacionEntity GetDataUsuarioLogin(DboUsuarioEntity dboUsuarioEntity, ref List<DboAccionEntity> menu);
        DboUsuarioEntity GetUserByClaimsPrincipal(ClaimsPrincipal claimsPrincipal);
        string DecryptByClaimsPrincipal(string data, ClaimsPrincipal claimsPrincipal);
        string EcryptByClaimsPrincipal(string data, ClaimsPrincipal claimsPrincipal);
        bool GetPeticionPermiso(DboUsuarioEntity dboUsuarioEntity, string controlador, string metodoAccion, string metodoHttp, int? idAccion);
    }
}