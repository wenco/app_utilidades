using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboAccionService : IDboAccionService
    {
        private IUnitOfWorkRepository uow;
        public DboAccionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboAccionEntity> GetPagination(RequestPaginationModel<DboAccionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboAccionEntity> result = this.uow.dboAccionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboAccionEntity> GetList(DboAccionQueryData queryData)
        {
            List<DboAccionEntity> result = this.uow.dboAccionRepository.GetList(queryData);
            return result;
        }

        public DboAccionEntity GetOne(DboAccionQueryData queryData)
        {
            DboAccionEntity result = this.uow.dboAccionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboAccionEntity> Delete(DboAccionEntity DboAccionEntity)
        {
            this.uow.dboAccionRepository.Reload(DboAccionEntity);
            this.uow.dboAccionRepository.Remove(DboAccionEntity);
            return new ResponseRequestModel<DboAccionEntity>()
            {
                Data = DboAccionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionEntity> Create(DboAccionEntity DboAccionEntity)
        {
            this.uow.dboAccionRepository.Add(DboAccionEntity);
            return new ResponseRequestModel<DboAccionEntity>()
            {
                Data = DboAccionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionEntity> Update(DboAccionEntity DboAccionEntity, DboAccionUpdateModel dboAccionUpdateModel)
        {
            this.uow.dboAccionRepository.Reload(DboAccionEntity);
            this.uow.dboAccionRepository.UpdateInMemory<DboAccionEntity, DboAccionUpdateModel>(DboAccionEntity, dboAccionUpdateModel);
            this.uow.dboAccionRepository.Update(DboAccionEntity);
            return new ResponseRequestModel<DboAccionEntity>()
            {
                Data = DboAccionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

