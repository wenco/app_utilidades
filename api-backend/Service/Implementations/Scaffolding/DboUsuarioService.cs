using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;
using api.Data.CustomQuery.QueryEnum;
using System;
using api.Utils;

namespace api.Service.Implementations.Scaffolding
{
    public class DboUsuarioService : IDboUsuarioService
    {
        private IUnitOfWorkRepository uow;
        public DboUsuarioService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboUsuarioEntity> GetPagination(RequestPaginationModel<DboUsuarioQueryData> requestPagination)
        {
            ResponsePaginationModel<DboUsuarioEntity> result = this.uow.dboUsuarioRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboUsuarioEntity> GetList(DboUsuarioQueryData queryData)
        {
            List<DboUsuarioEntity> result = this.uow.dboUsuarioRepository.GetList(queryData);
            return result;
        }

        public DboUsuarioEntity GetOne(DboUsuarioQueryData queryData)
        {
            DboUsuarioEntity result = this.uow.dboUsuarioRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboUsuarioEntity> Delete(DboUsuarioEntity DboUsuarioEntity)
        {
            this.uow.dboUsuarioRepository.Reload(DboUsuarioEntity);
            this.uow.dboUsuarioRepository.Remove(DboUsuarioEntity);
            return new ResponseRequestModel<DboUsuarioEntity>()
            {
                Data = DboUsuarioEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboUsuarioEntity> Create(DboUsuarioEntity DboUsuarioEntity)
        {
            this.uow.dboUsuarioRepository.Add(DboUsuarioEntity);
            return new ResponseRequestModel<DboUsuarioEntity>()
            {
                Data = DboUsuarioEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
        public ResponseRequestModel<DboUsuarioEntity> Update(DboUsuarioEntity dboUsuarioEntity)
        {
            this.uow.dboUsuarioRepository.Update(dboUsuarioEntity);
            return new ResponseRequestModel<DboUsuarioEntity>()
            {
                Data = dboUsuarioEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
        public ResponseRequestModel<DboUsuarioEntity> UpdateFromModel(DboUsuarioEntity DboUsuarioEntity, DboUsuarioUpdateModel dboUsuarioUpdateModel)
        {
            this.uow.dboUsuarioRepository.Reload(DboUsuarioEntity);
            this.uow.dboUsuarioRepository.UpdateInMemory<DboUsuarioEntity, DboUsuarioUpdateModel>(DboUsuarioEntity, dboUsuarioUpdateModel);
            this.uow.dboUsuarioRepository.Update(DboUsuarioEntity);
            return new ResponseRequestModel<DboUsuarioEntity>()
            {
                Data = DboUsuarioEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public DboUsuarioEntity AsignarLoginData(SettingsConfiguration settingsConfiguration, int idUsuario, string Token = null, DateTime? fechaExpiracion = null)
        {
            DboUsuarioEntity dboUsuarioEntity = this.GetOne(new DboUsuarioQueryData()
            {
                TipoFiltro = DboUsuarioQueryEnum.BusquedaPorIdUsuario,
                IdUsuario = idUsuario
            });
            if (fechaExpiracion != null) dboUsuarioEntity.FechaExpiracion = fechaExpiracion;
            if (Token != null) dboUsuarioEntity.Token = Token;
            dboUsuarioEntity.TokenPublico = HashUtil.RandomString(20);

            this.Update(dboUsuarioEntity);

            return dboUsuarioEntity;
        }
    }
}

