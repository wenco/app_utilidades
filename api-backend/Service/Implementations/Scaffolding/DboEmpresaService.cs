using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboEmpresaService : IDboEmpresaService
    {
        private IUnitOfWorkRepository uow;
        public DboEmpresaService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboEmpresaEntity> GetPagination(RequestPaginationModel<DboEmpresaQueryData> requestPagination)
        {
            ResponsePaginationModel<DboEmpresaEntity> result = this.uow.dboEmpresaRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboEmpresaEntity> GetList(DboEmpresaQueryData queryData)
        {
            List<DboEmpresaEntity> result = this.uow.dboEmpresaRepository.GetList(queryData);
            return result;
        }

        public DboEmpresaEntity GetOne(DboEmpresaQueryData queryData)
        {
            DboEmpresaEntity result = this.uow.dboEmpresaRepository.GetOne(queryData);
            return result;
        }
        
        public void Reload(DboEmpresaEntity dboEmpresa)
        {
            this.uow.dboEmpresaRepository.Reload(dboEmpresa);
        }

        public ResponseRequestModel<DboEmpresaEntity> Delete(DboEmpresaEntity DboEmpresaEntity)
        {
            this.uow.dboEmpresaRepository.Reload(DboEmpresaEntity);
            this.uow.dboEmpresaRepository.Remove(DboEmpresaEntity);
            return new ResponseRequestModel<DboEmpresaEntity>()
            {
                Data = DboEmpresaEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboEmpresaEntity> Create(DboEmpresaEntity DboEmpresaEntity)
        {
            this.uow.dboEmpresaRepository.Add(DboEmpresaEntity);
            return new ResponseRequestModel<DboEmpresaEntity>()
            {
                Data = DboEmpresaEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboEmpresaEntity> Update(DboEmpresaEntity DboEmpresaEntity, DboEmpresaUpdateModel dboEmpresaUpdateModel)
        {
            this.uow.dboEmpresaRepository.Reload(DboEmpresaEntity);
            this.uow.dboEmpresaRepository.UpdateInMemory<DboEmpresaEntity, DboEmpresaUpdateModel>(DboEmpresaEntity, dboEmpresaUpdateModel);
            this.uow.dboEmpresaRepository.Update(DboEmpresaEntity);
            return new ResponseRequestModel<DboEmpresaEntity>()
            {
                Data = DboEmpresaEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

