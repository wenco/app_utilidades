using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboRolService : IDboRolService
    {
        private IUnitOfWorkRepository uow;
        public DboRolService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboRolEntity> GetPagination(RequestPaginationModel<DboRolQueryData> requestPagination)
        {
            ResponsePaginationModel<DboRolEntity> result = this.uow.dboRolRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboRolEntity> GetList(DboRolQueryData queryData)
        {
            List<DboRolEntity> result = this.uow.dboRolRepository.GetList(queryData);
            return result;
        }

        public DboRolEntity GetOne(DboRolQueryData queryData)
        {
            DboRolEntity result = this.uow.dboRolRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboRolEntity> Delete(DboRolEntity DboRolEntity)
        {
            this.uow.dboRolRepository.Reload(DboRolEntity);
            this.uow.dboRolRepository.Remove(DboRolEntity);
            return new ResponseRequestModel<DboRolEntity>()
            {
                Data = DboRolEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolEntity> Create(DboRolEntity DboRolEntity)
        {
            this.uow.dboRolRepository.Add(DboRolEntity);
            return new ResponseRequestModel<DboRolEntity>()
            {
                Data = DboRolEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolEntity> Update(DboRolEntity DboRolEntity, DboRolUpdateModel dboRolUpdateModel)
        {
            this.uow.dboRolRepository.Reload(DboRolEntity);
            this.uow.dboRolRepository.UpdateInMemory<DboRolEntity, DboRolUpdateModel>(DboRolEntity, dboRolUpdateModel);
            this.uow.dboRolRepository.Update(DboRolEntity);
            return new ResponseRequestModel<DboRolEntity>()
            {
                Data = DboRolEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

