using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboRolUsuarioAplicacionService : IDboRolUsuarioAplicacionService
    {
        private IUnitOfWorkRepository uow;
        public DboRolUsuarioAplicacionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboRolUsuarioAplicacionEntity> GetPagination(RequestPaginationModel<DboRolUsuarioAplicacionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboRolUsuarioAplicacionEntity> result = this.uow.dboRolUsuarioAplicacionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboRolUsuarioAplicacionEntity> GetList(DboRolUsuarioAplicacionQueryData queryData)
        {
            List<DboRolUsuarioAplicacionEntity> result = this.uow.dboRolUsuarioAplicacionRepository.GetList(queryData);
            return result;
        }

        public DboRolUsuarioAplicacionEntity GetOne(DboRolUsuarioAplicacionQueryData queryData)
        {
            DboRolUsuarioAplicacionEntity result = this.uow.dboRolUsuarioAplicacionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboRolUsuarioAplicacionEntity> Delete(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity)
        {
            this.uow.dboRolUsuarioAplicacionRepository.Reload(DboRolUsuarioAplicacionEntity);
            this.uow.dboRolUsuarioAplicacionRepository.Remove(DboRolUsuarioAplicacionEntity);
            return new ResponseRequestModel<DboRolUsuarioAplicacionEntity>()
            {
                Data = DboRolUsuarioAplicacionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolUsuarioAplicacionEntity> Create(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity)
        {
            this.uow.dboRolUsuarioAplicacionRepository.Add(DboRolUsuarioAplicacionEntity);
            return new ResponseRequestModel<DboRolUsuarioAplicacionEntity>()
            {
                Data = DboRolUsuarioAplicacionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolUsuarioAplicacionEntity> Update(DboRolUsuarioAplicacionEntity DboRolUsuarioAplicacionEntity, DboRolUsuarioAplicacionUpdateModel dboRolUsuarioAplicacionUpdateModel)
        {
            this.uow.dboRolUsuarioAplicacionRepository.Reload(DboRolUsuarioAplicacionEntity);
            this.uow.dboRolUsuarioAplicacionRepository.UpdateInMemory<DboRolUsuarioAplicacionEntity, DboRolUsuarioAplicacionUpdateModel>(DboRolUsuarioAplicacionEntity, dboRolUsuarioAplicacionUpdateModel);
            this.uow.dboRolUsuarioAplicacionRepository.Update(DboRolUsuarioAplicacionEntity);
            return new ResponseRequestModel<DboRolUsuarioAplicacionEntity>()
            {
                Data = DboRolUsuarioAplicacionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

