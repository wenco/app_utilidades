using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboAccionUsuarioService : IDboAccionUsuarioService
    {
        private IUnitOfWorkRepository uow;
        public DboAccionUsuarioService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboAccionUsuarioEntity> GetPagination(RequestPaginationModel<DboAccionUsuarioQueryData> requestPagination)
        {
            ResponsePaginationModel<DboAccionUsuarioEntity> result = this.uow.dboAccionUsuarioRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboAccionUsuarioEntity> GetList(DboAccionUsuarioQueryData queryData)
        {
            List<DboAccionUsuarioEntity> result = this.uow.dboAccionUsuarioRepository.GetList(queryData);
            return result;
        }

        public DboAccionUsuarioEntity GetOne(DboAccionUsuarioQueryData queryData)
        {
            DboAccionUsuarioEntity result = this.uow.dboAccionUsuarioRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboAccionUsuarioEntity> Delete(DboAccionUsuarioEntity DboAccionUsuarioEntity)
        {
            this.uow.dboAccionUsuarioRepository.Reload(DboAccionUsuarioEntity);
            this.uow.dboAccionUsuarioRepository.Remove(DboAccionUsuarioEntity);
            return new ResponseRequestModel<DboAccionUsuarioEntity>()
            {
                Data = DboAccionUsuarioEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionUsuarioEntity> Create(DboAccionUsuarioEntity DboAccionUsuarioEntity)
        {
            this.uow.dboAccionUsuarioRepository.Add(DboAccionUsuarioEntity);
            return new ResponseRequestModel<DboAccionUsuarioEntity>()
            {
                Data = DboAccionUsuarioEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionUsuarioEntity> Update(DboAccionUsuarioEntity DboAccionUsuarioEntity, DboAccionUsuarioUpdateModel dboAccionUsuarioUpdateModel)
        {
            this.uow.dboAccionUsuarioRepository.Reload(DboAccionUsuarioEntity);
            this.uow.dboAccionUsuarioRepository.UpdateInMemory<DboAccionUsuarioEntity, DboAccionUsuarioUpdateModel>(DboAccionUsuarioEntity, dboAccionUsuarioUpdateModel);
            this.uow.dboAccionUsuarioRepository.Update(DboAccionUsuarioEntity);
            return new ResponseRequestModel<DboAccionUsuarioEntity>()
            {
                Data = DboAccionUsuarioEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

