using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboAplicacionService : IDboAplicacionService
    {
        private IUnitOfWorkRepository uow;
        public DboAplicacionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboAplicacionEntity> GetPagination(RequestPaginationModel<DboAplicacionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboAplicacionEntity> result = this.uow.dboAplicacionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboAplicacionEntity> GetList(DboAplicacionQueryData queryData)
        {
            List<DboAplicacionEntity> result = this.uow.dboAplicacionRepository.GetList(queryData);
            return result;
        }

        public DboAplicacionEntity GetOne(DboAplicacionQueryData queryData)
        {
            DboAplicacionEntity result = this.uow.dboAplicacionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboAplicacionEntity> Delete(DboAplicacionEntity DboAplicacionEntity)
        {
            this.uow.dboAplicacionRepository.Reload(DboAplicacionEntity);
            this.uow.dboAplicacionRepository.Remove(DboAplicacionEntity);
            return new ResponseRequestModel<DboAplicacionEntity>()
            {
                Data = DboAplicacionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAplicacionEntity> Create(DboAplicacionEntity DboAplicacionEntity)
        {
            this.uow.dboAplicacionRepository.Add(DboAplicacionEntity);
            return new ResponseRequestModel<DboAplicacionEntity>()
            {
                Data = DboAplicacionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAplicacionEntity> Update(DboAplicacionEntity DboAplicacionEntity, DboAplicacionUpdateModel dboAplicacionUpdateModel)
        {
            this.uow.dboAplicacionRepository.Reload(DboAplicacionEntity);
            this.uow.dboAplicacionRepository.UpdateInMemory<DboAplicacionEntity, DboAplicacionUpdateModel>(DboAplicacionEntity, dboAplicacionUpdateModel);
            this.uow.dboAplicacionRepository.Update(DboAplicacionEntity);
            return new ResponseRequestModel<DboAplicacionEntity>()
            {
                Data = DboAplicacionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

