using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboAccionPeticionService : IDboAccionPeticionService
    {
        private IUnitOfWorkRepository uow;
        public DboAccionPeticionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboAccionPeticionEntity> GetPagination(RequestPaginationModel<DboAccionPeticionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboAccionPeticionEntity> result = this.uow.dboAccionPeticionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboAccionPeticionEntity> GetList(DboAccionPeticionQueryData queryData)
        {
            List<DboAccionPeticionEntity> result = this.uow.dboAccionPeticionRepository.GetList(queryData);
            return result;
        }

        public DboAccionPeticionEntity GetOne(DboAccionPeticionQueryData queryData)
        {
            DboAccionPeticionEntity result = this.uow.dboAccionPeticionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboAccionPeticionEntity> Delete(DboAccionPeticionEntity DboAccionPeticionEntity)
        {
            this.uow.dboAccionPeticionRepository.Reload(DboAccionPeticionEntity);
            this.uow.dboAccionPeticionRepository.Remove(DboAccionPeticionEntity);
            return new ResponseRequestModel<DboAccionPeticionEntity>()
            {
                Data = DboAccionPeticionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionPeticionEntity> Create(DboAccionPeticionEntity DboAccionPeticionEntity)
        {
            this.uow.dboAccionPeticionRepository.Add(DboAccionPeticionEntity);
            return new ResponseRequestModel<DboAccionPeticionEntity>()
            {
                Data = DboAccionPeticionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboAccionPeticionEntity> Update(DboAccionPeticionEntity DboAccionPeticionEntity, DboAccionPeticionUpdateModel dboAccionPeticionUpdateModel)
        {
            this.uow.dboAccionPeticionRepository.Reload(DboAccionPeticionEntity);
            this.uow.dboAccionPeticionRepository.UpdateInMemory<DboAccionPeticionEntity, DboAccionPeticionUpdateModel>(DboAccionPeticionEntity, dboAccionPeticionUpdateModel);
            this.uow.dboAccionPeticionRepository.Update(DboAccionPeticionEntity);
            return new ResponseRequestModel<DboAccionPeticionEntity>()
            {
                Data = DboAccionPeticionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

