using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;
using api.Data.CustomQuery.QueryEnum;
using System.Linq;
using System;

namespace api.Service.Implementations.Scaffolding
{
    public class DboPeticionService : IDboPeticionService
    {
        private IUnitOfWorkRepository uow;
        public DboPeticionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboPeticionEntity> GetPagination(RequestPaginationModel<DboPeticionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboPeticionEntity> result = this.uow.dboPeticionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboPeticionEntity> GetList(DboPeticionQueryData queryData)
        {
            List<DboPeticionEntity> result = this.uow.dboPeticionRepository.GetList(queryData);
            return result;
        }

        public DboPeticionEntity GetOne(DboPeticionQueryData queryData)
        {
            DboPeticionEntity result = this.uow.dboPeticionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboPeticionEntity> Delete(DboPeticionEntity DboPeticionEntity)
        {
            this.uow.dboPeticionRepository.Reload(DboPeticionEntity);
            this.uow.dboPeticionRepository.Remove(DboPeticionEntity);
            return new ResponseRequestModel<DboPeticionEntity>()
            {
                Data = DboPeticionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboPeticionEntity> Create(DboPeticionEntity DboPeticionEntity)
        {
            this.uow.dboPeticionRepository.Add(DboPeticionEntity);
            return new ResponseRequestModel<DboPeticionEntity>()
            {
                Data = DboPeticionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboPeticionEntity> Update(DboPeticionEntity DboPeticionEntity, DboPeticionUpdateModel dboPeticionUpdateModel)
        {
            this.uow.dboPeticionRepository.Reload(DboPeticionEntity);
            this.uow.dboPeticionRepository.UpdateInMemory<DboPeticionEntity, DboPeticionUpdateModel>(DboPeticionEntity, dboPeticionUpdateModel);
            this.uow.dboPeticionRepository.Update(DboPeticionEntity);
            return new ResponseRequestModel<DboPeticionEntity>()
            {
                Data = DboPeticionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public void CrearPeticiones(List<DboPeticionEntity> dboPeticionList, SettingsConfiguration settingsConfiguration)
        {
            // obtener app
            DboAplicacionEntity dboAplicacionEntity = this.uow.dboAplicacionRepository.GetOne(new DboAplicacionQueryData()
            {
                TipoFiltro = DboAplicacionQueryEnum.SinIncludeBusquedaPorAliasApp,
                aliasApp = settingsConfiguration.AliasApp
            });

            // buscar por aplicacion
            List<DboPeticionEntity> ActualDboPeticionList = this.uow.dboPeticionRepository.GetList(
                new DboPeticionQueryData()
                {
                    TipoFiltro = DboPeticionQueryEnum.SinIncludeBusquedaPorAplicacion,
                    aliasApp = settingsConfiguration.AliasApp
                }
            );

            try
            {
                this.uow.BeginTransaction();

                foreach (var item in dboPeticionList)
                {
                    if (ActualDboPeticionList.Where(
                            x => x.Controller == item.Controller &&
                                 x.Action == item.Action &&
                                 x.Method == item.Method).Count() == 0
                    )// Si la petición no existe en la base de datos, se debe crear
                    {
                        item.IdAplicacion = dboAplicacionEntity.IdAplicacion;
                        this.uow.dboPeticionRepository.Add(item);
                    }
                }

                ActualDboPeticionList = this.uow.dboPeticionRepository.GetList(
                    new DboPeticionQueryData()
                    {
                        TipoFiltro = DboPeticionQueryEnum.SinIncludeBusquedaPorAplicacion,
                        aliasApp = settingsConfiguration.AliasApp
                    }
                );

                foreach (var item in ActualDboPeticionList)
                {
                    if (dboPeticionList.Where(
                            x => x.Controller == item.Controller &&
                                 x.Action == item.Action &&
                                 x.Method == item.Method).Count() == 0
                    )// Si existe en la base de datos y no existe en la lista actual, se debe borrar
                    {//enviara error si esta asociada a accion, por lo que se tiene eliminar manualmente la inconsistencia
                        this.uow.dboPeticionRepository.Remove(item);
                    }
                }

                this.uow.CommitTransaction();
            }
            catch (Exception)
            {
                this.uow.RollbackTransaction();
                throw;
            }
        }
    }
}

