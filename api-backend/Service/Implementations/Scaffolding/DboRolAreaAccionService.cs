using System.Collections.Generic;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Data.CustomQuery;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Scaffolding
{
    public class DboRolAreaAccionService : IDboRolAreaAccionService
    {
        private IUnitOfWorkRepository uow;
        public DboRolAreaAccionService(
            IUnitOfWorkRepository uow
        )
        {
            this.uow = uow;
        }

        public ResponsePaginationModel<DboRolAreaAccionEntity> GetPagination(RequestPaginationModel<DboRolAreaAccionQueryData> requestPagination)
        {
            ResponsePaginationModel<DboRolAreaAccionEntity> result = this.uow.dboRolAreaAccionRepository.GetPagination(requestPagination);
            return result;
        }

        public List<DboRolAreaAccionEntity> GetList(DboRolAreaAccionQueryData queryData)
        {
            List<DboRolAreaAccionEntity> result = this.uow.dboRolAreaAccionRepository.GetList(queryData);
            return result;
        }

        public DboRolAreaAccionEntity GetOne(DboRolAreaAccionQueryData queryData)
        {
            DboRolAreaAccionEntity result = this.uow.dboRolAreaAccionRepository.GetOne(queryData);
            return result;
        }

        public ResponseRequestModel<DboRolAreaAccionEntity> Delete(DboRolAreaAccionEntity DboRolAreaAccionEntity)
        {
            this.uow.dboRolAreaAccionRepository.Reload(DboRolAreaAccionEntity);
            this.uow.dboRolAreaAccionRepository.Remove(DboRolAreaAccionEntity);
            return new ResponseRequestModel<DboRolAreaAccionEntity>()
            {
                Data = DboRolAreaAccionEntity,
                Text = "Eliminado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolAreaAccionEntity> Create(DboRolAreaAccionEntity DboRolAreaAccionEntity)
        {
            this.uow.dboRolAreaAccionRepository.Add(DboRolAreaAccionEntity);
            return new ResponseRequestModel<DboRolAreaAccionEntity>()
            {
                Data = DboRolAreaAccionEntity,
                Text = "Creado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }

        public ResponseRequestModel<DboRolAreaAccionEntity> Update(DboRolAreaAccionEntity DboRolAreaAccionEntity, DboRolAreaAccionUpdateModel dboRolAreaAccionUpdateModel)
        {
            this.uow.dboRolAreaAccionRepository.Reload(DboRolAreaAccionEntity);
            this.uow.dboRolAreaAccionRepository.UpdateInMemory<DboRolAreaAccionEntity, DboRolAreaAccionUpdateModel>(DboRolAreaAccionEntity, dboRolAreaAccionUpdateModel);
            this.uow.dboRolAreaAccionRepository.Update(DboRolAreaAccionEntity);
            return new ResponseRequestModel<DboRolAreaAccionEntity>()
            {
                Data = DboRolAreaAccionEntity,
                Text = "Modificado de forma correcta",
                ResponseRequest = ResponseRequestEnum.Success
            };
        }
    }
}

