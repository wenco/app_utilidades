using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using api.Data.CustomEnums;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Data.Models;
using api.Repository.Interfaces;
using api.Service.Interfaces;
using api.Service.Interfaces.Base;
using api.Utils;
using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace api.Service.Implementations.Base
{
    public class AuthService : IAuthService
    {
        private IUnitOfWorkRepository unitOfWorkRepository;
        private readonly SettingsConfiguration settingsConfiguration;
        public AuthService(
            IUnitOfWorkRepository unitOfWorkRepository,
            SettingsConfiguration settingsConfiguration
        )
        {
            this.unitOfWorkRepository = unitOfWorkRepository;
            this.settingsConfiguration = settingsConfiguration;
        }
        public DboRolUsuarioAplicacionEntity GetDataUsuarioLogin(DboUsuarioEntity dboUsuarioEntity, ref List<DboAccionEntity> menu)
        {
            // verificar que el usuario exista
            dboUsuarioEntity = this.unitOfWorkRepository.dboUsuarioRepository.GetOne(
                new DboUsuarioQueryData()
                {
                    TipoFiltro = DboUsuarioQueryEnum.BusquedaPorAdSamAccountName,
                    AdSamAccountName = dboUsuarioEntity.AdSamAccountName
                }
            );

            if (dboUsuarioEntity == null)
            {
                throw new Exception("No existe usuario (3)");
            }

            if (dboUsuarioEntity.Activo != YesNoEnum.Yes)
            {
                throw new Exception("Usuario está inactivo");
            }

            string AliasApp = this.settingsConfiguration.AliasApp;

            if (AliasApp == null)
            {
                throw new Exception("Sin alias de aplicación asignado");
            }

            DboRolUsuarioAplicacionEntity dboRolUsuarioAplicacionEntity = this.unitOfWorkRepository.dboRolUsuarioAplicacionRepository.GetOne(
                new DboRolUsuarioAplicacionQueryData()
                {
                    TipoFiltro = DboRolUsuarioAplicacionQueryEnum.BuscarPorUsuarioAplicacion,
                    AliasApp = AliasApp,
                    IdUsuario = dboUsuarioEntity.IdUsuario
                }
            );

            if (dboRolUsuarioAplicacionEntity == null)
            {
                throw new Exception("Usuario no asignado a aplicación");
            }

            if (dboUsuarioEntity.IdArea == null)
            {
                throw new Exception("Usuario sin area asignada");
            }

            // acciones por usuario
            List<DboAccionUsuarioEntity> dboAccionUsuarioList = this.unitOfWorkRepository.dboAccionUsuarioRepository.GetList(
                new DboAccionUsuarioQueryData()
                {
                    TipoFiltro = DboAccionUsuarioQueryEnum.BuscarPorUsuarioAplicacion,
                    IdUsuario = dboUsuarioEntity.IdUsuario,
                    IdAplicacion = dboRolUsuarioAplicacionEntity.IdAplicacion
                }
            );

            // acciones por area
            List<DboRolAreaAccionEntity> dboRolAreaAccionList = this.unitOfWorkRepository.dboRolAreaAccionRepository.GetList(
                new DboRolAreaAccionQueryData()
                {
                    TipoFiltro = DboRolAreaAccionQueryEnum.BusquedaPorAreaRolIncludeAccion,
                    IdArea = dboUsuarioEntity.IdArea.Value,
                    IdRol = dboRolUsuarioAplicacionEntity.IdRol,
                    IdAplicacion = dboRolUsuarioAplicacionEntity.IdAplicacion
                }
            );

            List<int> accionSinPermisos = dboAccionUsuarioList.Where(x => x.TienePermiso == YesNoEnum.No).Select(x => x.IdAccion).ToList();
            // ignorar acciones sin permisos del listado de permisos por area
            dboRolAreaAccionList = dboRolAreaAccionList.Where(x => !accionSinPermisos.Contains(x.IdAccion)).ToList();
            // ignorar acciones sin permisos del listado de permisos por usuario
            dboAccionUsuarioList = dboAccionUsuarioList.Where(x => x.TienePermiso == YesNoEnum.Yes).ToList();
            // unificar listado de acciones por area y por usuario
            menu = dboRolAreaAccionList.Select(x => x.FkRolAreaAccionAccion).Union(dboAccionUsuarioList.Select(x => x.FkAccionUsuarioAccion)).ToList();

            // if (menu.Count() == 0)
            // {
            //     throw new Exception("Usuario sin acciones/menu configurado");
            // }
            return dboRolUsuarioAplicacionEntity;
        }

        public DboUsuarioEntity GetUserByClaimsPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            var user = claimsPrincipal.Claims.FirstOrDefault(x => x.Type == "IdUsuario");
            if (user == null)
            {
                throw new Exception("No existe usuario (1)");
            }

            DboUsuarioEntity dboUsuarioEntity = this.unitOfWorkRepository.dboUsuarioRepository.GetOne(new DboUsuarioQueryData()
            {
                TipoFiltro = DboUsuarioQueryEnum.BusquedaPorIdUsuario,
                IdUsuario = Convert.ToInt32(user.Value)
            });

            //Temporal
            //TODO: mejorar
            this.unitOfWorkRepository.dboUsuarioRepository.Reload(dboUsuarioEntity);

            if (dboUsuarioEntity == null)
            {
                throw new Exception("No existe usuario (2)");
            }

            return dboUsuarioEntity;
        }

        public string DecryptByClaimsPrincipal(string data, ClaimsPrincipal claimsPrincipal)
        {
            string token = this.GetUserByClaimsPrincipal(claimsPrincipal).Token;
            return ClsCrypto.OpenSSLDecrypt(data, token);
        }

        public string EcryptByClaimsPrincipal(string data, ClaimsPrincipal claimsPrincipal)
        {
            string token = this.GetUserByClaimsPrincipal(claimsPrincipal).Token;
            string dataEncript = ClsCrypto.OpenSSLEncrypt(data, token);

            return dataEncript;
        }

        public bool GetPeticionPermiso(DboUsuarioEntity dboUsuarioEntity, string controlador, string metodoAccion, string metodoHttp, int? idAccion)
        {
            DboPeticionEntity dboPeticionEntity = this.unitOfWorkRepository.dboPeticionRepository.GetOne(
                new DboPeticionQueryData()
                {
                    TipoFiltro = DboPeticionQueryEnum.SinIncludeBusquedaPeticion,
                    Controlador = controlador,
                    MetodoAccion = metodoAccion,
                    MetodoHttp = metodoHttp
                }
            );

            if (dboPeticionEntity == null)
            {
                return false;
            }

            if (!Convert.ToBoolean(dboPeticionEntity.Restriccion))
            {
                return true;
            }

            // en este punto el usuario debe existir o la petición esta inconsistente
            if (dboUsuarioEntity == null)
            {
                return false;
            }

            // sesion terminada
            if (dboUsuarioEntity.FechaExpiracion < DateTime.Now)
            {
                return false;
            }

            DboAccionEntity dboAccionEntity = this.unitOfWorkRepository.dboAccionRepository.GetOne(
                new DboAccionQueryData()
                {
                    TipoFiltro = DboAccionQueryEnum.BusquedaPorIdAccion,
                    IdAccion = Convert.ToInt32(idAccion)
                }
            );

            if (dboAccionEntity == null)
            {
                return false;
            }

            DboAccionPeticionEntity dboAccionPeticionEntity = this.unitOfWorkRepository.dboAccionPeticionRepository.GetOne(
                new DboAccionPeticionQueryData()
                {
                    TipoFiltro = DboAccionPeticionQueryEnum.SinIncludeBusquedaPorAccionPeticion,
                    IdAccion = dboAccionEntity.IdAccion,
                    IdPeticion = dboPeticionEntity.IdPeticion
                }
            );

            if (dboAccionPeticionEntity == null)
            {
                return false;
            }

            List<DboAccionEntity> menu = new List<DboAccionEntity>();
            DboRolUsuarioAplicacionEntity dboRolUsuarioAplicacionEntity = this.GetDataUsuarioLogin(dboUsuarioEntity, ref menu);

            if (dboRolUsuarioAplicacionEntity == null)
            {
                return false;
            }

            return menu.Where(x => x.IdAccion == dboAccionEntity.IdAccion).Count() > 0;
        }
    }
}