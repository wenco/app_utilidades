using api.Repository.Interfaces;
using api.Service.Implementations.Scaffolding;
using api.Service.Interfaces;
using api.Service.Interfaces.Base;
using api.Service.Interfaces.Scaffolding;

namespace api.Service.Implementations.Base
{
    public class UnitOfWorkService : IUnitOfWorkService
    {
        private IUnitOfWorkRepository _unitOfWorkRepository;
        #region Declarar
        private IDboUsuarioService _dboUsuarioService;
        private IDboRolUsuarioAplicacionService _dboRolUsuarioAplicacionService;
        private IDboRolService _dboRolService;
        private IDboRolAreaAccionService _dboRolAreaAccionService;
        private IDboPeticionService _dboPeticionService;
        private IDboAplicacionService _dboAplicacionService;
        private IDboAccionUsuarioService _dboAccionUsuarioService;
        private IDboAccionPeticionService _dboAccionPeticionService;
        private IDboAccionService _dboAccionService;
        private IDboEmpresaService _dboEmpresaService;
        #endregion
        private IAuthService _authService;
        private readonly SettingsConfiguration _settingsConfiguration;
        public UnitOfWorkService(
            IUnitOfWorkRepository _unitOfWorkRepository,
            SettingsConfiguration _settingsConfiguration
        )
        {
            this._unitOfWorkRepository = _unitOfWorkRepository;
            this._settingsConfiguration = _settingsConfiguration;
        }

        #region Inyectar
        public IDboUsuarioService dboUsuarioService
        {
            get { return this._dboUsuarioService = this._dboUsuarioService ?? new DboUsuarioService(this._unitOfWorkRepository); }
        }

        public IDboRolUsuarioAplicacionService dboRolUsuarioAplicacionService
        {
            get { return this._dboRolUsuarioAplicacionService = this._dboRolUsuarioAplicacionService ?? new DboRolUsuarioAplicacionService(this._unitOfWorkRepository); }
        }

        public IDboRolService dboRolService
        {
            get { return this._dboRolService = this._dboRolService ?? new DboRolService(this._unitOfWorkRepository); }
        }

        public IDboRolAreaAccionService dboRolAreaAccionService
        {
            get { return this._dboRolAreaAccionService = this._dboRolAreaAccionService ?? new DboRolAreaAccionService(this._unitOfWorkRepository); }
        }

        public IDboPeticionService dboPeticionService
        {
            get { return this._dboPeticionService = this._dboPeticionService ?? new DboPeticionService(this._unitOfWorkRepository); }
        }

        public IDboAplicacionService dboAplicacionService
        {
            get { return this._dboAplicacionService = this._dboAplicacionService ?? new DboAplicacionService(this._unitOfWorkRepository); }
        }

        public IDboAccionUsuarioService dboAccionUsuarioService
        {
            get { return this._dboAccionUsuarioService = this._dboAccionUsuarioService ?? new DboAccionUsuarioService(this._unitOfWorkRepository); }
        }

        public IDboAccionPeticionService dboAccionPeticionService
        {
            get { return this._dboAccionPeticionService = this._dboAccionPeticionService ?? new DboAccionPeticionService(this._unitOfWorkRepository); }
        }

        public IDboAccionService dboAccionService
        {
            get { return this._dboAccionService = this._dboAccionService ?? new DboAccionService(this._unitOfWorkRepository); }
        }
        public IDboEmpresaService dboEmpresaService
        {
            get { return this._dboEmpresaService = this._dboEmpresaService ?? new DboEmpresaService(this._unitOfWorkRepository); }
        }

        #endregion

        public IAuthService authService
        {
            get { return this._authService = this._authService ?? new AuthService(this._unitOfWorkRepository, this._settingsConfiguration); }
        }

    }
}
