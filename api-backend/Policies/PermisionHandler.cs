using System;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System.Linq;
using api.Service.Interfaces.Base;
using api.Data.Entities;

namespace api.Policies
{

    public class PermisionHandler : AuthorizationHandler<PermisionRequirement>
    {
        private readonly IServiceScopeFactory serviceScopeFactory;
        private JsonSerializerSettings serializerSettings = new JsonSerializerSettings();
        public IConfiguration Configuration;
        private IUnitOfWorkService uow;
        public PermisionHandler(
            IServiceScopeFactory serviceScopeFactory,
            IConfiguration Configuration
        )
        {
            this.serviceScopeFactory = serviceScopeFactory;
            this.Configuration = Configuration;
            serializerSettings.ContractResolver = new DefaultContractResolver();

            IServiceScope scope = this.serviceScopeFactory.CreateScope();
            this.uow = scope.ServiceProvider.GetRequiredService<IUnitOfWorkService>();
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context,
            PermisionRequirement requirement
        )
        {
            string recurso = context.Resource.ToString();
            if (recurso.Contains("/PrincipalSocketHub"))
            {
                context.Succeed(requirement);
            }
            else
            {

                AuthorizationFilterContext authorizationFilterContext = (AuthorizationFilterContext)context.Resource;

                string Controlador = authorizationFilterContext.RouteData.Values["controller"].ToString();
                string MetodoAccion = authorizationFilterContext.RouteData.Values["action"].ToString();
                string MetodoHttp = authorizationFilterContext.HttpContext.Request.Method;

                try
                {
                    if (!context.User.Identity.IsAuthenticated)
                    {
                        context.Fail();
                    }
                    else
                    {
                        if ((Controlador == "Push" && MetodoAccion == "RefreshNotifications" && MetodoHttp == "POST"))
                        {
                            context.Succeed(requirement);
                        }
                        else
                        {
                            string authRoute = null;

                            StringValues AuthRouteValue;
                            authorizationFilterContext.HttpContext.Request.Headers.TryGetValue("AuthRoute", out AuthRouteValue);
                            if (AuthRouteValue.Count() > 0)
                            {
                                authRoute = AuthRouteValue.FirstOrDefault();

                                int? IdAccion = Convert.ToInt32(this.uow.authService.DecryptByClaimsPrincipal(authRoute, context.User));

                                DboUsuarioEntity dboUsuarioEntity = this.uow.authService.GetUserByClaimsPrincipal(context.User);

                                bool tienePermiso = this.uow.authService.GetPeticionPermiso(dboUsuarioEntity, Controlador, MetodoAccion, MetodoHttp, IdAccion);

                                if (tienePermiso)
                                {
                                    context.Succeed(requirement);
                                }
                                else
                                {
                                    context.Fail();
                                }
                            }
                            else
                            {
                                context.Fail();
                            }


                        }

                    }
                }
                catch (System.Exception ex)
                {
                    context.Fail();
                }

            }


            return Task.CompletedTask;
        }
    }
}