using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using Microsoft.ApplicationInsights.Channel;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.Web;
using Microsoft.ApplicationInsights.Extensibility;

namespace api
{
    public class AzureInsightsTelemetryInitializers : ITelemetryInitializer
    {
        IHttpContextAccessor httpContextAccessor;

        public AzureInsightsTelemetryInitializers(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public void Initialize(ITelemetry telemetry)
        {
            if (httpContextAccessor.HttpContext != null)
            {
                try
                {

                    //      telemetry.Context.Properties["empresa"] = GetEmpresa();
                    telemetry.Context.GlobalProperties["clienteIp"] = GetClienteIp(telemetry);
                    telemetry.Context.GlobalProperties["serverIp"] = GetLocalIPAddress();
                    //    telemetry.Context.GlobalProperties["nombreUsuario"] = GetNombreUsuario();
                    telemetry.Context.GlobalProperties["authority"] = GetHostName();
                    telemetry.Context.GlobalProperties["requestQueryString"] = GetRequestQueryString();
                    telemetry.Context.GlobalProperties["requestForm"] = GetRequestForm();
                    //  telemetry.Context.GlobalProperties["tipoServidor"] = GetTipoServidor();
                }
                catch (Exception e)
                {
                    telemetry.Context.GlobalProperties["exception"] = e.Message;
                }
            }
        }


        private string GetClienteIp(ITelemetry telemetry)
        {
            try
            {
                if (telemetry.Context != null && telemetry.Context.Location != null)
                {
                    return telemetry.Context.Location.Ip;
                }
                else
                {
                    return "null";
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string GetLocalIPAddress()
        {
            try
            {
                var listIP = new List<string>();
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        listIP.Add(ip.ToString());
                    }
                }
                if (listIP.Count > 0)
                    return JsonConvert.SerializeObject(listIP);
                return "null";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string GetRequestQueryString()
        {
            try
            {
                if (httpContextAccessor.HttpContext.Request != null)
                {
                    if (httpContextAccessor.HttpContext.Request.QueryString != null)
                    {
                        var temp = new Dictionary<string, string>();
                        foreach (var q in httpContextAccessor.HttpContext.Request.Query)
                        {
                            temp[q.ToString()] = httpContextAccessor.HttpContext.Request.Query[q.ToString()];
                        }
                        return JsonConvert.SerializeObject(temp);
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string GetRequestForm()
        {
            try
            {
                if (httpContextAccessor.HttpContext.Request != null)
                {
                    if (httpContextAccessor.HttpContext.Request.Form != null)
                    {
                        var temp = new Dictionary<string, string>();
                        foreach (var q in httpContextAccessor.HttpContext.Request.Form)
                        {
                            temp[q.ToString()] = httpContextAccessor.HttpContext.Request.Form[q.ToString()];
                        }
                        return JsonConvert.SerializeObject(temp);
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private string GetHostName()
        {
            try
            {
                if (httpContextAccessor.HttpContext.Request != null && httpContextAccessor.HttpContext.Request.PathBase.Value != null)
                {
                    return httpContextAccessor.HttpContext.Request.PathBase.Value;
                }
                return "null";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}