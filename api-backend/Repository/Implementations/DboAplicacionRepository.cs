using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboAplicacionRepository : BaseRepository<DboAplicacionEntity>, IDboAplicacionRepository
    {
        private ContextoDb contextoDb;
        public DboAplicacionRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboAplicacionEntity> GetPagination(RequestPaginationModel<DboAplicacionQueryData> requestPagination)
        {
            IQueryable<DboAplicacionEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboAplicacionEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboAplicacionEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboAplicacionEntity> GetList(DboAplicacionQueryData queryData)
        {
            IQueryable<DboAplicacionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboAplicacionEntity GetOne(DboAplicacionQueryData queryData)
        {
            IQueryable<DboAplicacionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboAplicacionQueryEnum.SinIncludeBusquedaPorAliasApp:
                    q = q.Where(x => x.Alias == queryData.aliasApp);
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

