using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {

        #region Fields

        protected DbContext Context;
        public DbSet<T> DbSet;

        #endregion

        public BaseRepository(DbContext context)
        {
            Context = context;
            DbSet = context.Set<T>();
        }

        #region Public Methods

        public T GetById(long id) => DbSet.Find(id);

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
            => DbSet.FirstOrDefault(predicate);

        public void Add(T entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Remove(T entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public IEnumerable<T> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).ToList();
        }

        public int CountAll() => DbSet.Count();

        public int CountWhere(Expression<Func<T, bool>> predicate)
            => DbSet.Count(predicate);

        #endregion

        public void BeginTransaction()
        {
            Context.Database.BeginTransaction();
        }

        public void RollbackTransaction()
        {
            Context.Database.RollbackTransaction();
        }

        public void CommitTransaction()
        {
            Context.Database.CommitTransaction();
        }
        public virtual void Reload(T entity)
        {
            Context.Entry(entity).Reload();
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                throw new Exception("Entidad fue eliminada de la base de datos");
            }
        }

        public virtual void UpdateInMemory<F, U>(F entity, U model)
        {
            PropertyInfo[] entityPropertyInfo;
            entityPropertyInfo = typeof(F).GetProperties();

            PropertyInfo[] modelPropertyInfo;
            modelPropertyInfo = typeof(U).GetProperties();

            for (int i = 0; i < entityPropertyInfo.Length; i++)
            {
                PropertyInfo entityProperty = entityPropertyInfo[i];
                PropertyInfo modelProperty = modelPropertyInfo.Where(x =>
                    x.Name == entityProperty.Name &&
                    x.PropertyType.ToString() == entityProperty.PropertyType.ToString()
                ).FirstOrDefault();
                if (modelProperty != null)
                {
                    entity.GetType().GetProperty(entityProperty.Name).SetValue(
                        entity, model.GetType().GetProperty(entityProperty.Name).GetValue(model)
                    );
                }
            }
        }
    }
}