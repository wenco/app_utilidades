using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboAccionUsuarioRepository : BaseRepository<DboAccionUsuarioEntity>, IDboAccionUsuarioRepository
    {
        private ContextoDb contextoDb;
        public DboAccionUsuarioRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboAccionUsuarioEntity> GetPagination(RequestPaginationModel<DboAccionUsuarioQueryData> requestPagination)
        {
            IQueryable<DboAccionUsuarioEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboAccionUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboAccionUsuarioEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboAccionUsuarioEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboAccionUsuarioEntity> GetList(DboAccionUsuarioQueryData queryData)
        {
            IQueryable<DboAccionUsuarioEntity> q = this.DbSet;
            switch (queryData.TipoFiltro)
            {
                case DboAccionUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboAccionUsuarioQueryEnum.BuscarPorUsuarioAplicacion:
                    // solo las hojas del arbol pueden ser acciones o menu
                    // el resto siempre seran menu
                    q = q.Where(x => x.IdUsuario == queryData.IdUsuario &&
                                     x.FkAccionUsuarioAccion.IdAplicacion == queryData.IdAplicacion &&
                                     x.FkAccionUsuarioAccion.Tipo == TipoAccionEnum.Menu &&
                                     x.FkAccionUsuarioAccion.Activo == YesNoEnum.Yes)
                         .Include(x => x.FkAccionUsuarioAccion)
                         .ThenInclude(x => x.FkAccionAccionSuper)
                         .ThenInclude(x => x.FkAccionAccionSuper)
                         .ThenInclude(x => x.FkAccionAccionSuper); // maximo 4 niveles de profundidad
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboAccionUsuarioEntity GetOne(DboAccionUsuarioQueryData queryData)
        {
            IQueryable<DboAccionUsuarioEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboAccionUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

