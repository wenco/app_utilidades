using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboEmpresaRepository : BaseRepository<DboEmpresaEntity>, IDboEmpresaRepository
    {
        private ContextoDb contextoDb;
        public DboEmpresaRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboEmpresaEntity> GetPagination(RequestPaginationModel<DboEmpresaQueryData> requestPagination)
        {
            IQueryable<DboEmpresaEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboEmpresaQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboEmpresaEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboEmpresaEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboEmpresaEntity> GetList(DboEmpresaQueryData queryData)
        {
            IQueryable<DboEmpresaEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboEmpresaQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboEmpresaEntity GetOne(DboEmpresaQueryData queryData)
        {
            IQueryable<DboEmpresaEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboEmpresaQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

