using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboRolUsuarioAplicacionRepository : BaseRepository<DboRolUsuarioAplicacionEntity>, IDboRolUsuarioAplicacionRepository
    {
        private ContextoDb contextoDb;
        public DboRolUsuarioAplicacionRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboRolUsuarioAplicacionEntity> GetPagination(RequestPaginationModel<DboRolUsuarioAplicacionQueryData> requestPagination)
        {
            IQueryable<DboRolUsuarioAplicacionEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboRolUsuarioAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboRolUsuarioAplicacionEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboRolUsuarioAplicacionEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboRolUsuarioAplicacionEntity> GetList(DboRolUsuarioAplicacionQueryData queryData)
        {
            IQueryable<DboRolUsuarioAplicacionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboRolUsuarioAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboRolUsuarioAplicacionEntity GetOne(DboRolUsuarioAplicacionQueryData queryData)
        {
            IQueryable<DboRolUsuarioAplicacionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboRolUsuarioAplicacionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboRolUsuarioAplicacionQueryEnum.BuscarPorUsuarioAplicacion:
                    q = q.Where(x => x.IdUsuario == queryData.IdUsuario &&
                                     x.FkRolUsuarioAplicacionAplicacion.Alias == queryData.AliasApp)
                         .Include(x => x.FkRolUsuarioAplicacionAplicacion)
                         .Include(x => x.FkRolUsuarioAplicacionUsuario)
                         .Include(x => x.FkRolUsuarioAplicacionUsuario.FkUsuarioEmpresa);
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

