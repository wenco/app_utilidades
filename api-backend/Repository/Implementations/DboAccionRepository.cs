using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboAccionRepository : BaseRepository<DboAccionEntity>, IDboAccionRepository
    {
        private ContextoDb contextoDb;
        public DboAccionRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboAccionEntity> GetPagination(RequestPaginationModel<DboAccionQueryData> requestPagination)
        {
            IQueryable<DboAccionEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboAccionEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboAccionEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboAccionEntity> GetList(DboAccionQueryData queryData)
        {
            IQueryable<DboAccionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboAccionEntity GetOne(DboAccionQueryData queryData)
        {
            IQueryable<DboAccionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboAccionQueryEnum.BusquedaPorIdAccion:
                    q = q.Where(x => x.IdAccion == queryData.IdAccion);
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

