using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboRolAreaAccionRepository : BaseRepository<DboRolAreaAccionEntity>, IDboRolAreaAccionRepository
    {
        private ContextoDb contextoDb;
        public DboRolAreaAccionRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboRolAreaAccionEntity> GetPagination(RequestPaginationModel<DboRolAreaAccionQueryData> requestPagination)
        {
            IQueryable<DboRolAreaAccionEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboRolAreaAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboRolAreaAccionEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboRolAreaAccionEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboRolAreaAccionEntity> GetList(DboRolAreaAccionQueryData queryData)
        {
            IQueryable<DboRolAreaAccionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboRolAreaAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboRolAreaAccionQueryEnum.BusquedaPorAreaRolIncludeAccion:
                    q = q.Where(x => x.IdRol == queryData.IdRol &&
                                     x.IdArea == queryData.IdArea &&
                                     x.FkRolAreaAccionAccion.Tipo == TipoAccionEnum.Menu &&
                                     x.FkRolAreaAccionAccion.Activo == YesNoEnum.Yes &&
                                     x.FkRolAreaAccionAccion.FkAccionAplicacion.IdAplicacion == queryData.IdAplicacion)
                         .Include(x => x.FkRolAreaAccionAccion)
                         .ThenInclude(x => x.FkAccionAccionSuper)
                         .ThenInclude(x => x.FkAccionAccionSuper)
                         .ThenInclude(x => x.FkAccionAccionSuper); // maximo 4 niveles de profundidad
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboRolAreaAccionEntity GetOne(DboRolAreaAccionQueryData queryData)
        {
            IQueryable<DboRolAreaAccionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboRolAreaAccionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

