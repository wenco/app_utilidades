using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboPeticionRepository : BaseRepository<DboPeticionEntity>, IDboPeticionRepository
    {
        private ContextoDb contextoDb;
        public DboPeticionRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboPeticionEntity> GetPagination(RequestPaginationModel<DboPeticionQueryData> requestPagination)
        {
            IQueryable<DboPeticionEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboPeticionQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboPeticionEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboPeticionEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboPeticionEntity> GetList(DboPeticionQueryData queryData)
        {
            IQueryable<DboPeticionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboPeticionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboPeticionQueryEnum.SinIncludeBusquedaPorAplicacion:
                    q = q.Where(x =>
                        x.FkPeticionAplicacion.Alias == queryData.aliasApp
                    );
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboPeticionEntity GetOne(DboPeticionQueryData queryData)
        {
            IQueryable<DboPeticionEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboPeticionQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboPeticionQueryEnum.SinIncludeBusquedaPeticion:
                    q = q.Where(x =>
                        x.Controller == queryData.Controlador &&
                        x.Method == queryData.MetodoHttp &&
                        x.Action == queryData.MetodoAccion
                    );
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.FirstOrDefault();
        }
    }
}

