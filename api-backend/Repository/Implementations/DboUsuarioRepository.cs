using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace api.Repository.Implementations
{
    public class DboUsuarioRepository : BaseRepository<DboUsuarioEntity>, IDboUsuarioRepository
    {
        private ContextoDb contextoDb;
        public DboUsuarioRepository(ContextoDb contextoDb) : base(contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        public ResponsePaginationModel<DboUsuarioEntity> GetPagination(RequestPaginationModel<DboUsuarioQueryData> requestPagination)
        {
            IQueryable<DboUsuarioEntity> q = this.DbSet;

            switch (requestPagination.Filter.TipoFiltro)
            {
                case DboUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            if (requestPagination.Skip < 0 || requestPagination.Take < requestPagination.Skip)
            {
                throw new Exception("Skip debe ser mayor o igual a 0 y menor a Take");
            }

            if (requestPagination.Take <= 0)
            {
                throw new Exception("Skip debe ser mayor a 0");
            }

            // Retornar todos los registros
            if (requestPagination.Take == -1)
            {
                return new ResponsePaginationModel<DboUsuarioEntity>()
                {
                    Data = q.ToList(),
                    TotalData = q.Count()
                };
            }
            return new ResponsePaginationModel<DboUsuarioEntity>()
            {
                Data = q.Skip(requestPagination.Skip).Take(requestPagination.Take).ToList(),
                TotalData = q.Count()
            };
        }

        public List<DboUsuarioEntity> GetList(DboUsuarioQueryData queryData)
        {
            IQueryable<DboUsuarioEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList();
        }

        public DboUsuarioEntity GetOne(DboUsuarioQueryData queryData)
        {
            IQueryable<DboUsuarioEntity> q = this.DbSet;

            switch (queryData.TipoFiltro)
            {
                case DboUsuarioQueryEnum.SinIncludeSinFiltro:
                    break;
                case DboUsuarioQueryEnum.BusquedaPorTokenPublico:
                    q = q.Where(x => x.TokenPublico == queryData.TokenPublico);
                    break;
                case DboUsuarioQueryEnum.BusquedaPorIdUsuario:
                    q = q.Where(x => x.IdUsuario == queryData.IdUsuario && x.Activo == YesNoEnum.Yes);
                    break;
                case DboUsuarioQueryEnum.BusquedaPorAdSamAccountName:
                    q = q.Where(x => x.AdSamAccountName == queryData.AdSamAccountName)
                         .Include(x => x.FkUsuarioEmpresa);
                    break;
                default:
                    throw new Exception("Sin filtro definido");
            }

            return q.ToList().FirstOrDefault();
        }
    }
}

