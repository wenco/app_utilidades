using api.Data;
using api.Repository.Interfaces;

namespace api.Repository.Implementations
{
    public class UnitOfWorkRepository : IUnitOfWorkRepository
    {
        private readonly ContextoDb contextoDb;
        #region Declarar
        private IDboUsuarioRepository _dboUsuarioRepository;
        private IDboRolUsuarioAplicacionRepository _dboRolUsuarioAplicacionRepository;
        private IDboRolRepository _dboRolRepository;
        private IDboRolAreaAccionRepository _dboRolAreaAccionRepository;
        private IDboPeticionRepository _dboPeticionRepository;
        private IDboAplicacionRepository _dboAplicacionRepository;
        private IDboAccionUsuarioRepository _dboAccionUsuarioRepository;
        private IDboAccionPeticionRepository _dboAccionPeticionRepository;
        private IDboAccionRepository _dboAccionRepository;
        private IDboEmpresaRepository _dboEmpresaRepository;
        #endregion

        public UnitOfWorkRepository(ContextoDb contextoDb)
        {
            this.contextoDb = contextoDb;
        }

        #region Inyectar
        public IDboUsuarioRepository dboUsuarioRepository
        {
            get { return this._dboUsuarioRepository = this._dboUsuarioRepository ?? new DboUsuarioRepository(this.contextoDb); }
        }

        public IDboRolUsuarioAplicacionRepository dboRolUsuarioAplicacionRepository
        {
            get { return this._dboRolUsuarioAplicacionRepository = this._dboRolUsuarioAplicacionRepository ?? new DboRolUsuarioAplicacionRepository(this.contextoDb); }
        }

        public IDboRolRepository dboRolRepository
        {
            get { return this._dboRolRepository = this._dboRolRepository ?? new DboRolRepository(this.contextoDb); }
        }

        public IDboRolAreaAccionRepository dboRolAreaAccionRepository
        {
            get { return this._dboRolAreaAccionRepository = this._dboRolAreaAccionRepository ?? new DboRolAreaAccionRepository(this.contextoDb); }
        }

        public IDboPeticionRepository dboPeticionRepository
        {
            get { return this._dboPeticionRepository = this._dboPeticionRepository ?? new DboPeticionRepository(this.contextoDb); }
        }

        public IDboAplicacionRepository dboAplicacionRepository
        {
            get { return this._dboAplicacionRepository = this._dboAplicacionRepository ?? new DboAplicacionRepository(this.contextoDb); }
        }

        public IDboAccionUsuarioRepository dboAccionUsuarioRepository
        {
            get { return this._dboAccionUsuarioRepository = this._dboAccionUsuarioRepository ?? new DboAccionUsuarioRepository(this.contextoDb); }
        }

        public IDboAccionPeticionRepository dboAccionPeticionRepository
        {
            get { return this._dboAccionPeticionRepository = this._dboAccionPeticionRepository ?? new DboAccionPeticionRepository(this.contextoDb); }
        }

        public IDboAccionRepository dboAccionRepository
        {
            get { return this._dboAccionRepository = this._dboAccionRepository ?? new DboAccionRepository(this.contextoDb); }
        }
        public IDboEmpresaRepository dboEmpresaRepository
        {
            get { return this._dboEmpresaRepository = this._dboEmpresaRepository ?? new DboEmpresaRepository(this.contextoDb); }
        }

        #endregion

        public void SaveChanges()
        {
            this.contextoDb.SaveChanges();
        }

        public void Dispose()
        {
            this.contextoDb.Dispose();
        }

        public void BeginTransaction()
        {
            this.contextoDb.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            this.contextoDb.Database.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            this.contextoDb.Database.RollbackTransaction();
        }
    }
}
