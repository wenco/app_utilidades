using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboRolRepository : IBaseRepository<DboRolEntity>
    {
        ResponsePaginationModel<DboRolEntity> GetPagination(RequestPaginationModel<DboRolQueryData> requestPagination);
        List<DboRolEntity> GetList(DboRolQueryData queryData);
        DboRolEntity GetOne(DboRolQueryData queryData);
    }
}

