using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboAccionUsuarioRepository : IBaseRepository<DboAccionUsuarioEntity>
    {
        ResponsePaginationModel<DboAccionUsuarioEntity> GetPagination(RequestPaginationModel<DboAccionUsuarioQueryData> requestPagination);
        List<DboAccionUsuarioEntity> GetList(DboAccionUsuarioQueryData queryData);
        DboAccionUsuarioEntity GetOne(DboAccionUsuarioQueryData queryData);
    }
}

