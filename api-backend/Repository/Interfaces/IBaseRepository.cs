using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace api.Repository.Interfaces
{
    public interface IBaseRepository<T>
    {
        T GetById(long id);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);

        void Add(T entity);
        void Update(T entity);
        void Remove(T entity);

        IEnumerable<T> GetAll();
        IEnumerable<T> GetWhere(Expression<Func<T, bool>> predicate);

        int CountAll();
        int CountWhere(Expression<Func<T, bool>> predicate);

        void BeginTransaction();
        void RollbackTransaction();
        void CommitTransaction();
        void Reload(T entity);
        void UpdateInMemory<F, U>(F entity, U model);
    }
}