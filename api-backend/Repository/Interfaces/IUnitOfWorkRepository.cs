namespace api.Repository.Interfaces
{
    public interface IUnitOfWorkRepository
    {
        #region Declarar
        IDboUsuarioRepository dboUsuarioRepository { get; }
        IDboRolUsuarioAplicacionRepository dboRolUsuarioAplicacionRepository { get; }
        IDboRolRepository dboRolRepository { get; }
        IDboRolAreaAccionRepository dboRolAreaAccionRepository { get; }
        IDboPeticionRepository dboPeticionRepository { get; }
        IDboAplicacionRepository dboAplicacionRepository { get; }
        IDboAccionUsuarioRepository dboAccionUsuarioRepository { get; }
        IDboAccionPeticionRepository dboAccionPeticionRepository { get; }
        IDboAccionRepository dboAccionRepository { get; }
        IDboEmpresaRepository dboEmpresaRepository { get; }
        #endregion
        void SaveChanges();
        void Dispose();
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}
