using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboUsuarioRepository : IBaseRepository<DboUsuarioEntity>
    {
        ResponsePaginationModel<DboUsuarioEntity> GetPagination(RequestPaginationModel<DboUsuarioQueryData> requestPagination);
        List<DboUsuarioEntity> GetList(DboUsuarioQueryData queryData);
        DboUsuarioEntity GetOne(DboUsuarioQueryData queryData);
    }
}

