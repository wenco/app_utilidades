using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboAplicacionRepository : IBaseRepository<DboAplicacionEntity>
    {
        ResponsePaginationModel<DboAplicacionEntity> GetPagination(RequestPaginationModel<DboAplicacionQueryData> requestPagination);
        List<DboAplicacionEntity> GetList(DboAplicacionQueryData queryData);
        DboAplicacionEntity GetOne(DboAplicacionQueryData queryData);
    }
}

