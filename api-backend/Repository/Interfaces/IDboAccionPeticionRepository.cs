using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboAccionPeticionRepository : IBaseRepository<DboAccionPeticionEntity>
    {
        ResponsePaginationModel<DboAccionPeticionEntity> GetPagination(RequestPaginationModel<DboAccionPeticionQueryData> requestPagination);
        List<DboAccionPeticionEntity> GetList(DboAccionPeticionQueryData queryData);
        DboAccionPeticionEntity GetOne(DboAccionPeticionQueryData queryData);
    }
}

