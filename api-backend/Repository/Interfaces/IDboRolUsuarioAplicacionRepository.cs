using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboRolUsuarioAplicacionRepository : IBaseRepository<DboRolUsuarioAplicacionEntity>
    {
        ResponsePaginationModel<DboRolUsuarioAplicacionEntity> GetPagination(RequestPaginationModel<DboRolUsuarioAplicacionQueryData> requestPagination);
        List<DboRolUsuarioAplicacionEntity> GetList(DboRolUsuarioAplicacionQueryData queryData);
        DboRolUsuarioAplicacionEntity GetOne(DboRolUsuarioAplicacionQueryData queryData);
    }
}

