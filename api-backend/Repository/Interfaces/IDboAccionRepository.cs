using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboAccionRepository : IBaseRepository<DboAccionEntity>
    {
        ResponsePaginationModel<DboAccionEntity> GetPagination(RequestPaginationModel<DboAccionQueryData> requestPagination);
        List<DboAccionEntity> GetList(DboAccionQueryData queryData);
        DboAccionEntity GetOne(DboAccionQueryData queryData);
    }
}

