using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboRolAreaAccionRepository : IBaseRepository<DboRolAreaAccionEntity>
    {
        ResponsePaginationModel<DboRolAreaAccionEntity> GetPagination(RequestPaginationModel<DboRolAreaAccionQueryData> requestPagination);
        List<DboRolAreaAccionEntity> GetList(DboRolAreaAccionQueryData queryData);
        DboRolAreaAccionEntity GetOne(DboRolAreaAccionQueryData queryData);
    }
}

