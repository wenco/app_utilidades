using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboEmpresaRepository : IBaseRepository<DboEmpresaEntity>
    {
        ResponsePaginationModel<DboEmpresaEntity> GetPagination(RequestPaginationModel<DboEmpresaQueryData> requestPagination);
        List<DboEmpresaEntity> GetList(DboEmpresaQueryData queryData);
        DboEmpresaEntity GetOne(DboEmpresaQueryData queryData);
    }
}

