using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.Entities;
using api.Data.Models;

namespace api.Repository.Interfaces
{
    public interface IDboPeticionRepository : IBaseRepository<DboPeticionEntity>
    {
        ResponsePaginationModel<DboPeticionEntity> GetPagination(RequestPaginationModel<DboPeticionQueryData> requestPagination);
        List<DboPeticionEntity> GetList(DboPeticionQueryData queryData);
        DboPeticionEntity GetOne(DboPeticionQueryData queryData);
    }
}

