using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using api.Data;
using api.Data.AutoMapper;
using api.Data.CustomMapper;
using api.Hubs;
using api.Middleware;
using api.Policies;
using api.Repository.Implementations;
using api.Repository.Interfaces;
using api.Service.Implementations;
using api.Service.Implementations.Base;
using api.Service.Implementations.Scaffolding;
using api.Service.Interfaces;
using api.Service.Interfaces.Base;
using api.Service.Interfaces.Scaffolding;
using ElmahCore.Mvc;
using ElmahCore.Sql;
using FluentValidation;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace api
{
    public class Startup
    {

        public IConfiguration configuration { get; }
        public Startup(
            IConfiguration configuration
        )
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            ValidatorOptions.LanguageManager.Culture = new CultureInfo("es-CL");
            services.AddDbContext<ContextoDb>(x => x.UseSqlServer(this.configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("api")));

            services.AddElmah<SqlErrorLog>(options =>
            {
                options.ConnectionString = this.configuration.GetConnectionString("DefaultConnection");
                options.CheckPermissionAction = RevisarHttpContext;
            });

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                #region MapperConfiguration
                cfg.AddProfile(new DboUsuarioProfile());
                cfg.AddProfile(new DboRolUsuarioAplicacionProfile());
                cfg.AddProfile(new DboRolProfile());
                cfg.AddProfile(new DboRolAreaAccionProfile());
                cfg.AddProfile(new DboPeticionProfile());
                cfg.AddProfile(new DboAplicacionProfile());
                cfg.AddProfile(new DboAccionUsuarioProfile());
                cfg.AddProfile(new DboAccionPeticionProfile());
                cfg.AddProfile(new DboAccionProfile());
                #endregion
                cfg.AddProfile(new UsuarioAuthProfile());

            });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            /* Espacio para registrar repositorios */
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IUnitOfWorkRepository, UnitOfWorkRepository>();
            services.AddScoped<IUnitOfWorkService, UnitOfWorkService>();
            //----------------------------------------------------------------------
            #region ScaffoldingRepository
            services.AddTransient<IDboUsuarioRepository, DboUsuarioRepository>();
            services.AddTransient<IDboRolUsuarioAplicacionRepository, DboRolUsuarioAplicacionRepository>();
            services.AddTransient<IDboRolRepository, DboRolRepository>();
            services.AddTransient<IDboRolAreaAccionRepository, DboRolAreaAccionRepository>();
            services.AddTransient<IDboPeticionRepository, DboPeticionRepository>();
            services.AddTransient<IDboAplicacionRepository, DboAplicacionRepository>();
            services.AddTransient<IDboAccionUsuarioRepository, DboAccionUsuarioRepository>();
            services.AddTransient<IDboAccionPeticionRepository, DboAccionPeticionRepository>();
            services.AddTransient<IDboAccionRepository, DboAccionRepository>();
            services.AddTransient<IDboEmpresaRepository, DboEmpresaRepository>();
            #endregion

            services.AddSingleton<ITelemetryInitializer, AzureInsightsTelemetryInitializers>();
            services.AddApplicationInsightsTelemetry();

            /* Espacio para registrar servicios */
            #region ScaffoldingService
            services.AddTransient<IDboUsuarioService, DboUsuarioService>();
            services.AddTransient<IDboRolUsuarioAplicacionService, DboRolUsuarioAplicacionService>();
            services.AddTransient<IDboRolService, DboRolService>();
            services.AddTransient<IDboRolAreaAccionService, DboRolAreaAccionService>();
            services.AddTransient<IDboPeticionService, DboPeticionService>();
            services.AddTransient<IDboAplicacionService, DboAplicacionService>();
            services.AddTransient<IDboAccionUsuarioService, DboAccionUsuarioService>();
            services.AddTransient<IDboAccionPeticionService, DboAccionPeticionService>();
            services.AddTransient<IDboAccionService, DboAccionService>();
            services.AddTransient<IDboEmpresaService, DboEmpresaService>();
            #endregion
            services.AddTransient<IAuthService, AuthService>();

            services.Add(new ServiceDescriptor(typeof(IActionResultExecutor<JsonResult>),
                    Type.GetType("Microsoft.AspNetCore.Mvc.Infrastructure.SystemTextJsonResultExecutor, Microsoft.AspNetCore.Mvc.Core"),
                    ServiceLifetime.Singleton
                )
            );

            var settingsConfiguration = new SettingsConfiguration();
            new ConfigureFromConfigurationOptions<SettingsConfiguration>(this.configuration.GetSection("SettingsConfiguration")).Configure(settingsConfiguration);
            services.AddSingleton(settingsConfiguration);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = settingsConfiguration.Jwt.AuthIssuer,
                    ValidAudience = settingsConfiguration.Jwt.AuthAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settingsConfiguration.Jwt.AuthSecret))
                };
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        if (context.Request.Path.Value.StartsWith("/PrincipalSocketHub") &&
                            context.Request.Query.TryGetValue("token", out StringValues token)
                        )
                        {
                            context.Token = token;
                        }

                        return Task.CompletedTask;
                    },
                    OnAuthenticationFailed = context =>
                    {
                        var te = context.Exception;
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddControllers()
                    .AddNewtonsoftJson(options =>
                    {
                        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    });

            services.AddMvc(option => option.EnableEndpointRouting = false);

            services.AddSignalR();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("PermisionPolicy", policy => policy.Requirements.Add(new PermisionRequirement()));
            });

            services.AddSingleton<IAuthorizationHandler, PermisionHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.MapWhen(context => context.Request.Path.StartsWithSegments("/elmah", StringComparison.OrdinalIgnoreCase), appBuilder => { appBuilder.UseMiddleware<ElmahMiddleware>(); appBuilder.UseElmah(); });
            // app.UseStaticFiles(new StaticFileOptions()
            // {
            //     FileProvider = new PhysicalFileProvider(
            //                 Path.Combine(Directory.GetCurrentDirectory(), @"Static")),
            //     RequestPath = new PathString("/Static")
            // });

            app.UseCors(builder => builder
                .AllowAnyHeader()
                .AllowAnyMethod()
                .SetIsOriginAllowed(_ => true)
                .AllowCredentials()
            );

            app.UseRouting();

            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();

            app.UseElmah();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<PrincipalSocketHub>("/PrincipalSocketHub");
            });
        }

        private bool RevisarHttpContext(HttpContext httpContext)
        {
            return true;
            // httpContext.Request.Query.TryGetValue("token", out StringValues token);
            // if (token.ToString() == "1234")
            // {
            //     return true;
            // }

            // return false;
        }
    }
}
