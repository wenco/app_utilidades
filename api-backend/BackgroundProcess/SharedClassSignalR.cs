using System;
using System.Collections.Generic;
using System.Linq;
using api.Data.CustomModels;
using api.Data.Models;
using api.Utils;

namespace api.BackgroundProcess
{
    public class SharedClassSignalR
    {
        private static List<ClientSocketModel> listaSocketClient = new List<ClientSocketModel>();
        private static object mutex = new object();

        public static void AgregarElementoSocketClient(string ConnectionId, DboUsuarioReadModel DboUsuario)
        {
            lock (mutex)
            {
                listaSocketClient.Add(new ClientSocketModel()
                {
                    ConnectionId = ConnectionId,
                    DboUsuario = DboUsuario
                });
            }
        }

        public static void EliminarElementoSocketClient(string ConnectionId)
        {
            lock (mutex)
            {
                var item = listaSocketClient.Find(x => x.ConnectionId == ConnectionId);
                if (item != null)
                {
                    listaSocketClient.Remove(item);
                }
            }
        }

        public static List<ClientSocketModel> Clone()
        {
            List<ClientSocketModel> localListSocket = new List<ClientSocketModel>();
            lock (mutex)
            {
                localListSocket = CloneListUtil.Clone(SharedClassSignalR.listaSocketClient != null ? SharedClassSignalR.listaSocketClient : new List<ClientSocketModel>()).ToList();
            }
            return localListSocket;
        }

        public static List<ClientSocketModel> GetAllClientSocket()
        {
            lock (mutex)
            {
                return listaSocketClient;
            }
        }

        public static List<ClientSocketModel> GetClientsByUserId(int idUsuario)
        {
            lock (mutex)
            {
                return listaSocketClient.Where(x => x.DboUsuario.IdUsuario == idUsuario).ToList();
            }
        }
    }
}
