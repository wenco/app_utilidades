using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using api.Data.CustomEnums;
using api.Data.CustomModels;
using api.Data.CustomQuery;
using api.Data.CustomQuery.QueryEnum;
using api.Data.Entities;
using api.Data.Models;
using api.Service.Interfaces;
using api.Service.Interfaces.Base;
using api.Utils;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Novell.Directory.Ldap;

namespace api.Controllers
{
    public class AuthController : Controller
    {
        private IUnitOfWorkService uow;
        private IMapper mapper;
        private SettingsConfiguration settingsConfiguration;
        public AuthController(
            IUnitOfWorkService uow,
            IMapper mapper,
            SettingsConfiguration settingsConfiguration
        )
        {
            this.uow = uow;
            this.mapper = mapper;
            this.settingsConfiguration = settingsConfiguration;
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult PostLogin([FromBody] UsuarioAuthModel usuarioAuth)
        {
            try
            {
                if (usuarioAuth == null)
                {
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return Json("No existe usuario (1)");
                }
                DboUsuarioEntity dboUsuarioEntity = this.mapper.Map<DboUsuarioEntity>(usuarioAuth);

                List<DboAccionEntity> menu = new List<DboAccionEntity>();
                DboRolUsuarioAplicacionEntity dboRolUsuarioAplicacionEntity = this.uow.authService.GetDataUsuarioLogin(dboUsuarioEntity, ref menu);
                dboUsuarioEntity = dboRolUsuarioAplicacionEntity.FkRolUsuarioAplicacionUsuario;

                usuarioAuth = this.mapper.Map<UsuarioAuthModel>(dboUsuarioEntity);
                usuarioAuth.dboAplicacionReadModel = this.mapper.Map<DboAplicacionReadModel>(dboUsuarioEntity.FkRolUsuarioAplicacionUsuarioList.First().FkRolUsuarioAplicacionAplicacion);
                usuarioAuth.dboRolUsuarioAplicacionReadModelList = this.mapper.Map<List<DboRolUsuarioAplicacionReadModel>>(dboUsuarioEntity.FkRolUsuarioAplicacionUsuarioList);

                usuarioAuth.MenuList = this.mapper.Map<List<DboAccionReadModel>>(menu);

                // no cambiar token privado
                dboUsuarioEntity = this.uow.dboUsuarioService.AsignarLoginData(settingsConfiguration, dboUsuarioEntity.IdUsuario);

                // no enviar al front por seguridad
                usuarioAuth.TokenPublico = null;
                usuarioAuth.FechaExpiracion = dboUsuarioEntity.FechaExpiracion;
                usuarioAuth.Token = dboUsuarioEntity.Token;

                DboEmpresaEntity dboEmpresaActualEntity = this.uow.dboEmpresaService.GetOne(
                    new DboEmpresaQueryData()
                    {
                        TipoFiltro = DboEmpresaQueryEnum.BuscarEmpresaActual
                    }
                );
                usuarioAuth.dboEmpresaReadActual = this.mapper.Map<DboEmpresaReadModel>(dboEmpresaActualEntity);

                HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(usuarioAuth);
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
        }

        [NonAction]
        public bool ValidateUser(string domainName, string username, string password)
        {
            string userDn = $"{username}@{domainName}";
            var connection = new LdapConnection { SecureSocketLayer = false };
            connection.Connect(domainName, LdapConnection.DefaultPort);
            connection.Bind(userDn, password);
            return connection.Bound;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult PostLoginAdMovil([FromBody] UsuarioAuthModel usuarioAuth)
        {
            try
            {
                // mensajes de error genericos por seguridad
                if (usuarioAuth == null)
                {
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return Json("Usuario o contraseña son incorrectos (1)");
                }

                // verificar que exista cuenta en AD
                if (string.IsNullOrEmpty(usuarioAuth.AdSamAccountName) || string.IsNullOrEmpty(usuarioAuth.PassWord))
                {
                    throw new Exception("Usuario o contraseña son incorrectos (2)");
                }

                if (!ValidateUser(string.Format("{0}.local", "wenco"), usuarioAuth.AdSamAccountName, usuarioAuth.PassWord))
                {
                    throw new Exception("Usuario o contraseña son incorrectos (3)");
                }

                HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(usuarioAuth);
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult PostLoginAd([FromBody] UsuarioAuthModel usuarioAuth)
        {
            try
            {
                // mensajes de error genericos por seguridad
                if (usuarioAuth == null)
                {
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return Json("Usuario o contraseña son incorrectos (1)");
                }

                // verificar que exista cuenta en AD
                if (string.IsNullOrEmpty(usuarioAuth.AdSamAccountName) || string.IsNullOrEmpty(usuarioAuth.PassWord))
                {
                    throw new Exception("Usuario o contraseña son incorrectos (2)");
                }

                DboUsuarioEntity dboUsuarioEntity = this.uow.dboUsuarioService.GetOne(new DboUsuarioQueryData()
                {
                    TipoFiltro = DboUsuarioQueryEnum.BusquedaPorAdSamAccountName,
                    AdSamAccountName = usuarioAuth.AdSamAccountName
                });

                if (dboUsuarioEntity == null)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (3)");
                }

                if (dboUsuarioEntity.Activo == YesNoEnum.No)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (4)");
                }

                if (dboUsuarioEntity.IdEmpresaLogin == null)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (5)");
                }

                if (!ValidateUser(string.Format("{0}.local", dboUsuarioEntity.FkUsuarioEmpresa.Dominio), usuarioAuth.AdSamAccountName, usuarioAuth.PassWord))
                {
                    throw new Exception("Usuario o contraseña son incorrectos (6)");
                }

                usuarioAuth = this.mapper.Map<UsuarioAuthModel>(dboUsuarioEntity);

                var claims = new[]
                {
                    new Claim("IdUsuario", usuarioAuth.IdUsuario.ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Convert.ToString(this.settingsConfiguration.Jwt.AuthSecret)));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                DateTime fechaExpiracion = DateTime.Now.AddHours(settingsConfiguration.Jwt.HorasValidezLogin);

                JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                    issuer: this.settingsConfiguration.Jwt.AuthIssuer,
                    audience: this.settingsConfiguration.Jwt.AuthAudience,
                    claims: claims,
                    expires: fechaExpiracion,
                    signingCredentials: creds
                );
                string token = (new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken));
                dboUsuarioEntity = this.uow.dboUsuarioService.AsignarLoginData(settingsConfiguration, dboUsuarioEntity.IdUsuario, token, fechaExpiracion);

                HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(dboUsuarioEntity.TokenPublico);
            }
            catch (LdapException ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                if (ex.Message == "Invalid Credentials")
                {
                    return Json("Usuario o contraseña son incorrectos (7)");
                }
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [NonAction]
        public ActionResult PostLoginSinPass([FromBody] UsuarioAuthModel usuarioAuth)
        {
            try
            {
                // mensajes de error genericos por seguridad
                if (usuarioAuth == null)
                {
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return Json("Usuario o contraseña son incorrectos (1)");
                }

                // verificar que exista cuenta en AD
                if (string.IsNullOrEmpty(usuarioAuth.AdSamAccountName) || string.IsNullOrEmpty(usuarioAuth.PassWord))
                {
                    throw new Exception("Usuario o contraseña son incorrectos (2)");
                }

                DboUsuarioEntity dboUsuarioEntity = this.uow.dboUsuarioService.GetOne(new DboUsuarioQueryData()
                {
                    TipoFiltro = DboUsuarioQueryEnum.BusquedaPorAdSamAccountName,
                    AdSamAccountName = usuarioAuth.AdSamAccountName
                });

                if (dboUsuarioEntity == null)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (3)");
                }

                if (dboUsuarioEntity.Activo == YesNoEnum.No)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (4)");
                }

                if (dboUsuarioEntity.IdEmpresaLogin == null)
                {
                    throw new Exception("Usuario o contraseña son incorrectos (5)");
                }

                // if (!ValidateUser(string.Format("{0}.local", dboUsuarioEntity.FkUsuarioEmpresa.Dominio), usuarioAuth.AdSamAccountName, usuarioAuth.PassWord))
                // {
                //     throw new Exception("Usuario o contraseña son incorrectos (6)");
                // }

                usuarioAuth = this.mapper.Map<UsuarioAuthModel>(dboUsuarioEntity);

                var claims = new[]
                {
                    new Claim("IdUsuario", usuarioAuth.IdUsuario.ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Convert.ToString(this.settingsConfiguration.Jwt.AuthSecret)));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                DateTime fechaExpiracion = DateTime.Now.AddHours(settingsConfiguration.Jwt.HorasValidezLogin);

                JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                    issuer: this.settingsConfiguration.Jwt.AuthIssuer,
                    audience: this.settingsConfiguration.Jwt.AuthAudience,
                    claims: claims,
                    expires: fechaExpiracion,
                    signingCredentials: creds
                );
                string token = (new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken));
                dboUsuarioEntity = this.uow.dboUsuarioService.AsignarLoginData(settingsConfiguration, dboUsuarioEntity.IdUsuario, token, fechaExpiracion);

                HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(dboUsuarioEntity.TokenPublico);
            }
            catch (LdapException ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                if (ex.Message == "Invalid Credentials")
                {
                    return Json("Usuario o contraseña son incorrectos (7)");
                }
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult PostLoginTokenPublico([FromBody] UsuarioAuthModel usuarioAuth)
        {
            try
            {
                // mensajes de error genericos por seguridad
                if (usuarioAuth == null)
                {
                    HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                    return Json("Datos de login incorrectos (1)");
                }

                // verificar que exista cuenta en AD
                if (string.IsNullOrEmpty(usuarioAuth.TokenPublico))
                {
                    throw new Exception("TokenPublico incorrecto (1)");
                }

                DboUsuarioEntity dboUsuarioEntity = this.uow.dboUsuarioService.GetOne(new DboUsuarioQueryData()
                {
                    TipoFiltro = DboUsuarioQueryEnum.BusquedaPorTokenPublico,
                    TokenPublico = usuarioAuth.TokenPublico
                });

                if (dboUsuarioEntity == null)
                {
                    throw new Exception("TokenPublico incorrecto (2)");
                }

                if (dboUsuarioEntity.Activo == YesNoEnum.No)
                {
                    throw new Exception("TokenPublico incorrecto (3)");
                }

                if (dboUsuarioEntity.IdEmpresaLogin == null)
                {
                    throw new Exception("TokenPublico incorrecto (4)");
                }

                usuarioAuth = this.mapper.Map<UsuarioAuthModel>(dboUsuarioEntity);

                var claims = new[]
                {
                    new Claim("IdUsuario", usuarioAuth.IdUsuario.ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Convert.ToString(this.settingsConfiguration.Jwt.AuthSecret)));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                DateTime fechaExpiracion = DateTime.Now.AddHours(settingsConfiguration.Jwt.HorasValidezLogin);

                JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(
                    issuer: this.settingsConfiguration.Jwt.AuthIssuer,
                    audience: this.settingsConfiguration.Jwt.AuthAudience,
                    claims: claims,
                    expires: fechaExpiracion,
                    signingCredentials: creds
                );
                string token = (new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken));
                dboUsuarioEntity = this.uow.dboUsuarioService.AsignarLoginData(settingsConfiguration, dboUsuarioEntity.IdUsuario, token, fechaExpiracion);

                HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(dboUsuarioEntity.TokenPublico);
            }
            catch (LdapException ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                if (ex.Message == "Invalid Credentials")
                {
                    return Json("Usuario o contraseña son incorrectos (7)");
                }
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
            catch (System.Exception ex)
            {
                HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(ex.Message + (ex.InnerException == null ? "" : ", " + ex.InnerException.Message));
            }
        }
    }
}