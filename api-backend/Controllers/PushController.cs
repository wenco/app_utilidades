
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using api.BackgroundProcess;
using api.Data.CustomModels;
using api.Data.Entities;
using api.Hubs;
using api.Service.Interfaces.Base;
using api.Utils;
using ElmahCore;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace api.Controllers
{
    [Authorize("PermisionPolicy")]
    public class PushController : Controller
    {
        private IHubContext<PrincipalSocketHub> hubContext;
        private IUnitOfWorkService uow;
        public PushController(
            IHubContext<PrincipalSocketHub> hubContext,
            IUnitOfWorkService uow
        )
        {
            this.hubContext = hubContext;
            this.uow = uow;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetTest()
        {
            return Json("esto es una prueba");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetAllClientSocket()
        {
            List<ClientSocketModel> listaClientSocket = SharedClassSignalR.GetAllClientSocket().OrderByDescending(x => x.FechaUltimoMsg).ToList();
            HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
            return new JsonResult(listaClientSocket);
        }

        [HttpPost]
        public async Task<IActionResult> RefreshNotifications()
        {
            DboUsuarioEntity DboUsuario = this.uow.authService.GetUserByClaimsPrincipal(User);
            if (DboUsuario == null)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "No existe usuario valido en la base de datos");
            }

            List<ClientSocketModel> ListaClientsSocket = SharedClassSignalR.GetClientsByUserId(DboUsuario.IdUsuario);
            if (ListaClientsSocket.Count == 0)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "No existe cliente conectado para el usuario");
            }

            foreach (var item in ListaClientsSocket)
            {

                await this.hubContext.Clients.Client(item.ConnectionId).SendAsync("RefreshNotifications");
            }

            return StatusCode((int)HttpStatusCode.OK, "Refresh Exitoso");
        }
    }
}