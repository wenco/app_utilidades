
using System;
using api.Utils;
using ElmahCore;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace api.Controllers
{
    public class TestController : Controller
    {
        public TestController(
        )
        {
        }

        [HttpGet]
        public IActionResult GetTest()
        {
            return Json("esto es una prueba");
        }
    }
}