namespace api
{
    public sealed class SettingsConfiguration
    {
        public string AliasApp { get; set; }
        public JwtSettingsConfiguration Jwt { get; set; }
    }

    public sealed class JwtSettingsConfiguration
    {
        public int HorasValidezLogin { get; set; }
        public string AuthIssuer { get; set; }
        public string AuthAudience { get; set; }
        public string AuthSecret { get; set; }
    }
}