using System;
using System.Threading.Tasks;
using api.BackgroundProcess;
using api.Data.Entities;
using api.Data.Models;
using api.Service.Interfaces.Base;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;

namespace api.Hubs
{
    [Authorize("PermisionPolicy")]
    public class PrincipalSocketHub : Hub
    {
        private IUnitOfWorkService uow;
        private IMapper mapper;
        public PrincipalSocketHub(
            IUnitOfWorkService uow,
            IMapper mapper
        )
        {
            this.uow = uow;
            this.mapper = mapper;
        }
        public override Task OnConnectedAsync()
        {
            DboUsuarioEntity DboUsuario = this.uow.authService.GetUserByClaimsPrincipal(this.Context.User);
            DboUsuarioReadModel DboUsuarioRead = this.mapper.Map<DboUsuarioReadModel>(DboUsuario);
            SharedClassSignalR.AgregarElementoSocketClient(Context.ConnectionId, DboUsuarioRead);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            SharedClassSignalR.EliminarElementoSocketClient(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
    }
}