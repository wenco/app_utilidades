using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace Scaffolding
{
    public static class FrontEnd
    {
        static List<string> ListaCarpetas = new List<string>(){
            Program.RUTA_RAIZ+"api-backend\\Data\\CustomEnums",
            Program.RUTA_RAIZ+"api-backend\\Data\\CustomModels",
            Program.RUTA_RAIZ+"api-backend\\Data\\CustomQuery",
            Program.RUTA_RAIZ+"api-backend\\Data\\Models"
        };
        static string Destino = Program.RUTA_RAIZ + "api-frontend\\src\\app\\";

        static List<KeyValuePair<string, string>> defaultTypeTranslations = new List<KeyValuePair<string, string>>(){
            new KeyValuePair<string, string>("int", "number"),
            new KeyValuePair<string, string>("double", "number"),
            new KeyValuePair<string, string>("float", "number"),
            new KeyValuePair<string, string>("Int32", "number"),
            new KeyValuePair<string, string>("Int64", "number"),
            new KeyValuePair<string, string>("short", "number"),
            new KeyValuePair<string, string>("long", "number"),
            new KeyValuePair<string, string>("decimal", "number"),
            new KeyValuePair<string, string>("bool", "boolean"),
            new KeyValuePair<string, string>("DateTime", "string"),
            new KeyValuePair<string, string>("DateTimeOffset", "string"),
            new KeyValuePair<string, string>("Guid", "string"),
            new KeyValuePair<string, string>("dynamic", "any"),
            new KeyValuePair<string, string>("object", "any")
        };

        static List<string> datoList = new List<string>() {
            "List",
            "IEnumerable",
            "ICollection" };
        internal static void CopiarArchivosData()
        {
            Console.WriteLine(JsonConvert.SerializeObject(AllFolder(ListaCarpetas)));
            List<Archivo> listaArchivos = new List<Archivo>();
            foreach (var c in AllFolder(ListaCarpetas))
            {
                var archivos = Directory.GetFiles(c, "*.cs");
                foreach (var a in archivos)
                {
                    listaArchivos.Add(leerArchivo(a));
                }
            }
            foreach (var a in listaArchivos)
            {
                AgregarIncludeYActualizarClase(a, listaArchivos);
            }
            foreach (var a in listaArchivos)
            {
                Escribir(a);
            }
        }

        private static void Escribir(Archivo a)
        {
            Console.WriteLine("Generando archivo = " + a.rutaDestino);
            System.IO.FileInfo file = new System.IO.FileInfo(a.rutaDestino);
            file.Directory.Create(); // If the directory already exists, this method does nothing.
            using (StreamWriter writer = new StreamWriter(a.rutaDestino))
            {
                foreach (var item in a.contenido.Select(x => x.texto))
                {
                    writer.WriteLine(item);
                }
            }
        }

        static List<string> AllFolder(List<string> _ListaCarpetas)
        {
            var r = new List<string>();
            foreach (var c in _ListaCarpetas)
            {
                var folder = Directory.GetDirectories(c);
                if (folder.Length > 0)
                {
                    r.AddRange(AllFolder(folder.ToList()));
                }
                r.Add(c);
            }
            return r;
        }

        static Archivo leerArchivo(string ruta)
        {
            if (ruta.Contains("DboNotificacionReadModel"))
            {
                var a = 1;
            }
            List<Linea> contenido = new List<Linea>();
            string className = "";
            string classNameOriginal = "";

            using (StreamReader reader = new StreamReader(ruta))
            {
                string texto = "";
                bool ignorando = false;
                while ((texto = reader.ReadLine()) != null)
                {

                    Linea l = new Linea()
                    {
                        texto = texto
                    };
                    var coincidencia = Regex.Matches(l.texto, "using (.)*;");
                    if (coincidencia.Count == 1) continue;
                    coincidencia = Regex.Matches(l.texto, "namespace (.)*");
                    if (coincidencia.Count == 1) continue;
                    coincidencia = Regex.Matches(l.texto.Trim(), "// (.)*");
                    if (coincidencia.Count == 1) continue;

                    if (l.texto.Contains("#region noborrar"))
                    {
                        ignorando = true;
                        continue;
                    };

                    if (l.texto.Contains("#endregion noborrar"))
                    {
                        ignorando = false;
                        continue;
                    };

                    if (ignorando)
                    {
                        continue;
                    }

                    if (l.texto.Contains("#region")) continue;
                    if (l.texto.Contains("#endregion")) continue;

                    coincidencia = Regex.Matches(l.texto, "(.)*{ get; set; }(.)*");
                    if (coincidencia.Count == 0)
                    {
                        coincidencia = Regex.Matches(l.texto, "(.)*public virtual(.)*");
                    }
                    if (coincidencia.Count == 1)
                    {
                        l.texto = l.texto.Replace(" virtual ", " ");
                        l.texto = l.texto.Replace(" public ", "").Replace("{ get; set; }", "").Trim();
                        l.clase = l.texto.Split(" ")[0];
                        l.clase = l.clase.Split(".").Last();
                        var tipoList = datoList.Where(x => x == l.clase.Split("<")[0]).FirstOrDefault();
                        if (tipoList != null)
                        {
                            tipoList = l.clase.Replace(tipoList + "<", "").Replace(">", "") + "[]";
                            l.texto.Replace(l.clase, tipoList);
                            l.clase = tipoList;
                        }
                        l.texto = l.texto.Replace("?", "");
                        l.clase = l.clase.Replace("?", "");
                        var c = defaultTypeTranslations.Where(x => x.Key == l.clase || x.Key + "?" == l.clase).FirstOrDefault();
                        if (!c.Equals(default(KeyValuePair<string, string>)))
                        {
                            l.clase = c.Value;
                        }
                        l.texto = "    " + l.texto.Split(" ")[1] + ": " + l.clase + ";";
                        contenido.Add(l);
                        continue;
                    }

                    coincidencia = Regex.Matches(l.texto, "{(.)*");
                    if (coincidencia.Count == 1) continue;
                    coincidencia = Regex.Matches(l.texto, "(.)*}");
                    if (coincidencia.Count == 1) continue;
                    l.texto = "  " + l.texto.Trim();
                    coincidencia = Regex.Matches(l.texto, "(.)* (enum|class) (.)*");
                    if (coincidencia.Count == 1)
                    {
                        if (Regex.Matches(l.texto, "(.)* (class) (.)*").Count == 1)
                        {
                            l.texto = l.texto.Split(" ").Last();
                            var a = l.texto.Split("<");
                            classNameOriginal = a[0];
                            if (a[0].Substring(a[0].Length - 5, 5) == "Model")
                            {
                                l.texto = a[0].Substring(0, a[0].Length - 5) + "Interface";
                            }
                            else
                            {
                                l.texto = a[0] + "Interface";
                            }
                            if (a.Length > 1)
                            {
                                l.texto = l.texto + "<" + a[1];
                            }
                            className = l.texto;
                            l.texto = "export interface " + l.texto + " {";
                        }
                        else
                        {
                            l.texto = l.texto.Split(" ").Last();
                            className = l.texto;
                            classNameOriginal = l.texto;
                            l.texto = "export enum " + l.texto + " {";
                        }
                    }
                    if (string.IsNullOrEmpty(l.texto.Trim())) continue;
                    contenido.Add(l);
                }
                contenido.Add(new Linea() { texto = "}" });
            }

            var rutaDestino = GenerateRutaDestino(ruta, className);
            return new Archivo()
            {
                contenido = contenido,
                rutaOrigen = ruta,
                className = className,
                classNameOriginal = classNameOriginal,
                rutaDestino = Destino + rutaDestino,
                import = "..\\" + rutaDestino.Split("\\").Skip(1).Aggregate((x, y) => { return x + "\\" + y; }).Replace(".ts", "")
            };
        }

        private static string GenerateRutaDestino(string ruta, string className)
        {
            ruta = ruta.Replace(Program.RUTA_RAIZ + "api-backend\\", "");
            var rutaDestino = (ruta.Split("\\").ToList())
                            .Take(ruta.Split("\\").Length - 1)
                            .Aggregate((x, y) => { return x + "\\" + y; });
            rutaDestino = rutaDestino.Substring(0, 1).ToLower() + rutaDestino.Substring(1);
            for (int i = 1; i < rutaDestino.Length; i++)
            {
                if (Char.IsUpper(rutaDestino[i]))
                {
                    rutaDestino = rutaDestino.Substring(0, i)
                        + (Char.IsLetter(rutaDestino[i - 1]) ? "-" : "")
                        + rutaDestino.Substring(i, 1).ToLower()
                        + rutaDestino.Substring(i + 1);
                }
            }
            rutaDestino = rutaDestino + "\\" + GenerarNombreSalida(className);
            return rutaDestino;
        }

        private static void AgregarIncludeYActualizarClase(Archivo a, List<Archivo> listaArchivos)
        {
            if (a.rutaOrigen.Contains("DboRolUsuarioAplicacionReadModel.cs"))
            {
                var asdasd = "";
            }
            a.contenido.Insert(0, new Linea()
            {
                texto = "// " + a.rutaOrigen
            });
            if (a.className.Substring(a.className.Length - 4) != "Enum")
            {
                var import = listaArchivos.Where(x =>
                    a.contenido.Select(y => y.clase).ToList().Contains(x.classNameOriginal) ||
                    a.contenido.Select(y => y.clase).ToList().Contains(x.classNameOriginal + "[]")
                ).ToList();
                foreach (var item in import)
                {
                    if (a.classNameOriginal != item.classNameOriginal)
                    {
                        a.contenido.Insert(0, new Linea()
                        {
                            texto = "import { " + item.className + " } from '" + item.import.Replace("\\", "/") + "';"
                        });
                    }
                }
                foreach (var item in a.contenido)
                {
                    var t = import.Where(x => x.classNameOriginal == item.clase
                        || x.classNameOriginal + "[]" == item.clase).FirstOrDefault();
                    if (t != null)
                    {
                        item.texto = item.texto.Replace(t.classNameOriginal, t.className);
                    }
                }
            }
        }

        static string GenerarNombreSalida(string className)
        {
            int lastUpperCase = 0;
            className = className.Substring(0, 1).ToLower() + className.Substring(1);
            for (int i = 0; i < className.Length; i++)
            {
                if (Char.IsUpper(className[i]))
                {
                    className = className.Substring(0, i)
                        + "-"
                        + className.Substring(i, 1).ToLower()
                        + className.Substring(i + 1);
                    lastUpperCase = i;
                }
            }
            className = className.Substring(0, lastUpperCase)
                + "."
                + className.Substring(lastUpperCase + 1)
                + ".ts";
            var temp = className.Split("<");
            if (temp.Length > 1)
            {
                className = temp.First() + temp.Last().Split(">").Last();
            }
            return className;
        }

        private class Archivo
        {
            internal string import;
            internal string classNameOriginal;

            public List<Linea> contenido { get; set; }
            public string rutaOrigen { get; set; }
            public string rutaDestino { get; set; }
            public string className { get; set; }
        }
        private class Linea
        {
            public string texto { get; set; }
            public string clase { get; set; }
        }
    }
}