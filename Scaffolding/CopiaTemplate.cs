using System;
using System.Collections.Generic;
using System.IO;

namespace Scaffolding
{
    public class CopiaTemplate
    {
        static Dictionary<string, string> listaTemplate = new Dictionary<string, string>();

        public static void LecturaTemplate()
        {
            string directorio = @"templates\";
            string[] dirs = Directory.GetFiles(directorio, "*.cs_", SearchOption.AllDirectories);
            foreach (string dir in dirs)
            {
                var archivo = dir.Replace(directorio, "");
                FileStream fileStream = new FileStream(directorio + archivo, FileMode.Open);
                using (StreamReader reader = new StreamReader(fileStream))
                {
                    string contentFile = reader.ReadToEnd();
                    listaTemplate[archivo] = contentFile;
                }
            }
        }

        public static void EscrituraTemplate()
        {
            foreach (var e in Estructura.listaEstructura)
            {
                foreach (var t in listaTemplate)
                {
                    var f = Helpers.RemplazarTextoTemplate(t.Key, e).Replace(".cs_", ".cs");
                    f = Program.RUTA_RAIZ + f;
                    System.IO.FileInfo file = new System.IO.FileInfo(f);
                    file.Directory.Create(); // If the directory already exists, this method does nothing.
                    if (!File.Exists(f))
                    {
                        Console.WriteLine("Generando archivo = " + f);
                        using (StreamWriter writer = new StreamWriter(f))
                        {
                            string content = Helpers.RemplazarTextoTemplate(t.Value, e);
                            writer.WriteLine(content);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No se puede generar el archivo(ya existe) = " + f);
                    }
                }
            }
        }
    }
}