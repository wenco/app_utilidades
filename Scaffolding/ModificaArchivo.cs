using System.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace Scaffolding
{
    public class ModificaArchivo
    {
        static List<ConfiguracionArchivo> listaModificar = new List<ConfiguracionArchivo>()
        {
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Startup.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "MapperConfiguration",
                        template = new List<string>(){
                            "                cfg.AddProfile(new [__RemplazarAutoMapper__]Profile());"
                        }
                    },
                    new TemplateArchivo(){
                        region = "ScaffoldingRepository",
                        template = new List<string>(){
                            "            services.AddTransient<I[__RemplazarEntity__]Repository, [__RemplazarEntity__]Repository>();"
                        }
                    },
                    new TemplateArchivo(){
                        region = "ScaffoldingService",
                        template = new List<string>(){
                            "            services.AddTransient<I[__RemplazarEntity__]Service, [__RemplazarEntity__]Service>();"
                        }
                    }
                }
            },
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Repository\Implementations\UnitOfWorkRepository.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "Declarar",
                        template = new List<string>(){
                            "        private I[__RemplazarEntity__]Repository _[__remplazarEntity__]Repository;"
                        }
                    },
                    new TemplateArchivo(){
                        region = "Inyectar",
                        template = new List<string>(){
                            "        public I[__RemplazarEntity__]Repository [__remplazarEntity__]Repository",
                            "        {",
                            "            get { return this._[__remplazarEntity__]Repository = this._[__remplazarEntity__]Repository ?? new [__RemplazarEntity__]Repository(this.contextoDb); }",
                            "        }",
                            ""
                        }
                    }
                }
            },
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Repository\Interfaces\IUnitOfWorkRepository.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "Declarar",
                        template = new List<string>(){
                            "        I[__RemplazarEntity__]Repository [__remplazarEntity__]Repository { get; }"
                        }
                    }
                }
            },
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Service\Implementations\Base\UnitOfWorkService.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "Declarar",
                        template = new List<string>(){
                            "        private I[__RemplazarEntity__]Service _[__remplazarEntity__]Service;"
                        }
                    },
                    new TemplateArchivo(){
                        region = "Inyectar",
                        template = new List<string>(){
                            "        public I[__RemplazarEntity__]Service [__remplazarEntity__]Service",
                            "        {",
                            "            get { return this._[__remplazarEntity__]Service = this._[__remplazarEntity__]Service ?? new [__RemplazarEntity__]Service(this._unitOfWorkRepository); }",
                            "        }",
                            ""
                        }
                    }
                }
            },
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Service\Interfaces\Base\IUnitOfWorkService.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "Declarar",
                        template = new List<string>(){
                            "        I[__RemplazarEntity__]Service [__remplazarEntity__]Service { get; }"
                        }
                    }
                }
            },
            new ConfiguracionArchivo(){
                archivo = Program.RUTA_RAIZ+@"api-backend\Data\ModelValidation\UnitOfWorkModelValidador.cs",
                listaTemplate= new List<TemplateArchivo>(){
                    new TemplateArchivo(){
                        region = "Declarar",
                        template = new List<string>(){
                            "        private static [__RemplazarCreateValidation__]CreateModelValidator [__remplazarCreateValidation__]CreateModelValidator = new [__RemplazarCreateValidation__]CreateModelValidator();",
                            "        private static [__RemplazarUpdateValidation__]UpdateModelValidator [__remplazarUpdateValidation__]UpdateModelValidator = new [__RemplazarUpdateValidation__]UpdateModelValidator();",
                        }
                    },
                    new TemplateArchivo(){
                        region = "Validador",
                        template = new List<string>(){
                            "        public static ResponseRequestModel<[__RemplazarCreateModel__]CreateModel> Validate[__RemplazarCreateValidation__]CreateModelValidator([__RemplazarCreateModel__]CreateModel m)",
                            "        {",
                            "            ValidationResult results = [__remplazarCreateValidation__]CreateModelValidator.Validate(m);",
                            "            return Validador<[__RemplazarCreateModel__]CreateModel>(results, m);",
                            "        }",
                            "        public static ResponseRequestModel<[__RemplazarUpdateModel__]UpdateModel> Validate[__RemplazarUpdateValidation__]UpdateModelValidator([__RemplazarUpdateModel__]UpdateModel m)",
                            "        {",
                            "            ValidationResult results = [__remplazarUpdateValidation__]UpdateModelValidator.Validate(m);",
                            "            return Validador<[__RemplazarUpdateModel__]UpdateModel>(results, m);",
                            "        }",
                        }
                    },
                    new TemplateArchivo(){
                        isHelper= true,
                        region ="Helper",
                        template = new List<string>(){
                            "        private static ResponseRequestModel<M> Validador<M>(ValidationResult results, M model)",
                            "        {",
                            "            if (!results.IsValid)",
                            "            {",
                            "                return new ResponseRequestModel<M>()",
                            "                {",
                            "                    Text = results.Errors.Select(x => x.ErrorMessage).ToList()",
                            "                        .Aggregate((i, j) => i + \", \" + j),",
                            "                    ResponseRequest = ResponseRequestEnum.Warning",
                            "                };",
                            "            }",
                            "            else",
                            "            {",
                            "                return new ResponseRequestModel<M>()",
                            "                {",
                            "                    ResponseRequest = ResponseRequestEnum.Success",
                            "                };",
                            "            }",
                            "        }"
                        }
                    }
                }
            },
        };
        public static void RemplazarRegion()
        {
            foreach (var m in listaModificar)
            {
                List<string> file = new List<string>();
                using (StreamReader reader = new StreamReader(m.archivo))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        file.Add(line);
                    }
                }
                foreach (TemplateArchivo t in m.listaTemplate)
                {
                    file = ModificarArchivo(file, t);
                }
                using (StreamWriter writer = new StreamWriter(m.archivo))
                {
                    foreach (var item in file)
                    {
                        writer.WriteLine(item);
                    }
                }
            }
        }

        private static List<string> ModificarArchivo(List<string> file, TemplateArchivo t)
        {
            int inicioRegion = 0;
            for (int i = 0; i < file.Count; i++)
            {
                string l = file[i];
                if (inicioRegion == 0 && l.Contains("#region " + t.region))
                {
                    inicioRegion = i;
                    continue;
                }
                if (inicioRegion != 0)
                {
                    if (!l.Contains("#endregion"))
                    {
                        file.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        foreach (var entity in Estructura.listaEstructura)
                        {
                            List<string> lista = Helpers.RemplazarTextoTemplate(t.template, entity);
                            file.InsertRange(
                                inicioRegion + 1,
                                lista
                            );
                            if (t.isHelper)
                            {
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            return file;
        }
    }

    public class ConfiguracionArchivo
    {
        public string archivo;
        public List<TemplateArchivo> listaTemplate;
    }

    public class TemplateArchivo
    {
        public string region;
        public List<string> template;
        public bool isHelper = false;
    }
}