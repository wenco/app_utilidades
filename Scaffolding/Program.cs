using System;

namespace Scaffolding
{
    public class Program
    {
        public static string RUTA_RAIZ = @"..\";
        static void Main(string[] args)
        {
            try
            {
                Estructura.CargarEstructura();
                CopiaTemplate.LecturaTemplate();
                CopiaTemplate.EscrituraTemplate();
                ModificaArchivo.RemplazarRegion();
                FrontEnd.CopiarArchivosData();
                // Console.WriteLine("Scaffolding Generado");
            }
            catch (System.Exception ex)
            {
                var msg = ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "");
                Console.WriteLine(msg);
            }

        }
    }
}