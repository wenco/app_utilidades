using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Scaffolding
{
    public class Estructura
    {
        static string directorioEntities = Program.RUTA_RAIZ + @"api-backend\Data\Entities\";
        static string directorioModels = Program.RUTA_RAIZ + @"api-backend\Data\Models\";
        static string directorioAutoMapper = Program.RUTA_RAIZ + @"api-backend\Data\AutoMapper\";
        static string directorioModelValidation = Program.RUTA_RAIZ + @"api-backend\Data\ModelValidation\";
        public static List<EstructuraItem> listaEstructura = new List<EstructuraItem>();
        public static void CargarEstructura()
        {

            string[] dirs = Directory.GetFiles(directorioEntities, "*.cs");
            foreach (string dir in dirs)
            {
                string entity = dir.Replace(directorioEntities, "").Replace("Entity.cs", "");
                listaEstructura.Add(new EstructuraItem()
                {
                    entity = entity
                });
            }
            dirs = Directory.GetFiles(directorioModels, "*ReadModel.cs");
            foreach (var dir in dirs)
            {
                string model = dir.Replace(directorioModels, "").Replace("ReadModel.cs", "");
                EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == model.ToLower()).First();
                item.readModel = model;
            }
            dirs = Directory.GetFiles(directorioModels, "*CreateModel.cs");
            foreach (var dir in dirs)
            {
                string model = dir.Replace(directorioModels, "").Replace("CreateModel.cs", "");
                EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == model.ToLower()).First();
                item.createModel = model;
            }
            dirs = Directory.GetFiles(directorioModels, "*UpdateModel.cs");
            foreach (var dir in dirs)
            {
                string model = dir.Replace(directorioModels, "").Replace("UpdateModel.cs", "");
                EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == model.ToLower()).First();
                item.updateModel = model;
            }
            dirs = Directory.GetFiles(directorioAutoMapper, "*.cs");
            foreach (var dir in dirs)
            {
                string autoMapper = dir.Replace(directorioAutoMapper, "").Replace("Profile.cs", "");
                EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == autoMapper.ToLower()).First();
                item.autoMapper = autoMapper;
            }

            dirs = Directory.GetFiles(directorioAutoMapper, "*.cs");
            foreach (var dir in dirs)
            {
                string autoMapper = dir.Replace(directorioAutoMapper, "").Replace("Profile.cs", "");
                EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == autoMapper.ToLower()).First();
                item.autoMapper = autoMapper;
            }

            dirs = Directory.GetFiles(directorioModelValidation, "*.cs");
            foreach (var dir in dirs)
            {
                string autoMapper;
                if (dir.Contains("UnitOfWorkModelValidador.cs"))
                {
                    continue;
                }
                if (dir.Contains("CreateModelValidator.cs"))
                {
                    autoMapper = dir.Replace(directorioModelValidation, "").Replace("CreateModelValidator.cs", "");
                    EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == autoMapper.ToLower()).First();
                    item.createValidation = autoMapper;
                }
                else
                {
                    autoMapper = dir.Replace(directorioModelValidation, "").Replace("UpdateModelValidator.cs", "");
                    EstructuraItem item = listaEstructura.Where(x => x.entity.ToLower() == autoMapper.ToLower()).First();
                    item.updateValidation = autoMapper;
                }
            }
        }

        public class EstructuraItem
        {
            public string entity { get; set; }
            public string readModel { get; set; }
            public string createModel { get; set; }
            public string updateModel { get; set; }
            public string autoMapper { get; set; }
            public string createValidation { get; set; }
            public string updateValidation { get; set; }
        }
    }
}