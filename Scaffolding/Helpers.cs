using System.Linq;
using System;
using System.Collections.Generic;
using static Scaffolding.Estructura;

namespace Scaffolding
{
    public static class Helpers
    {
        private static string camelCase(string s)
        {
            return Char.ToLowerInvariant(s[0]) + s.Substring(1);
        }

        public static List<string> RemplazarTextoTemplate(List<string> textoList, EstructuraItem item)
        {
            textoList = textoList.Select(x => RemplazarTextoTemplate(x, item)).ToList();
            return textoList;
        }

        public static string RemplazarTextoTemplate(string texto, EstructuraItem item)
        {
            texto = texto.Replace("[__RemplazarEntity__]", item.entity);
            texto = texto.Replace("[__remplazarEntity__]", camelCase(item.entity));
            texto = texto.Replace("[__RemplazarReadModel__]", item.readModel);
            texto = texto.Replace("[__remplazarReadModel__]", camelCase(item.readModel));
            texto = texto.Replace("[__RemplazarCreateModel__]", item.createModel);
            texto = texto.Replace("[__remplazarCreateModel__]", camelCase(item.createModel));
            texto = texto.Replace("[__RemplazarUpdateModel__]", item.updateModel);
            texto = texto.Replace("[__remplazarUpdateModel__]", camelCase(item.updateModel));
            texto = texto.Replace("[__RemplazarCreateValidation__]", item.createValidation);
            texto = texto.Replace("[__remplazarCreateValidation__]", camelCase(item.createValidation));
            texto = texto.Replace("[__RemplazarUpdateValidation__]", item.updateValidation);
            texto = texto.Replace("[__remplazarUpdateValidation__]", camelCase(item.updateValidation));
            texto = texto.Replace("[__RemplazarAutoMapper__]", item.autoMapper);
            texto = texto.Replace("[__remplazarAutoMapper__]", camelCase(item.autoMapper));


            return texto;
        }
    }

}